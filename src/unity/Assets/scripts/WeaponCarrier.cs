﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WeaponCarrier
    : Moveable
{
    // members /////////////////////////////////////////////////////////////////
    public List<Weapon> Weapons = new List<Weapon>();

    // protected methods ///////////////////////////////////////////////////////
    protected void SynchWeapons()
    {
        for (int i = 0; i < Weapons.Count; i++)
        {
            Weapon weapon = Weapons[i];
            if (weapon.owner == null)
            {
                //Weapons.Remove(weapon);
                //AddWeapon(weapon.GetType());
                //break;
                weapon.owner = gameObject;
            }

            if(weapon.Instantiated == false)
            {
                Weapon newWeapon = Instantiate(weapon);
                Weapons.Remove(weapon);
                newWeapon.Instantiated = true;
                newWeapon.owner = gameObject;
                Weapons.Add(newWeapon);
                break;
            }
        }
    }

    protected void WeaponUpdates()
    {
        foreach (Weapon weapon in Weapons)
        {
            weapon.UpdateCooldown();
        }
    }

    protected Weapon AddWeapon<WeaponType>()
        where WeaponType : Weapon
    {
        WeaponType weapon = gameObject.AddComponent<WeaponType>();
        weapon.owner = this.gameObject;
        Weapons.Add(weapon);
        return weapon;
    }

    protected Weapon AddWeapon(System.Type weaponType)
    {
        Weapon weapon = gameObject.AddComponent(weaponType) as Weapon;
        if (weapon)
        {
            weapon.owner = this.gameObject;
            Weapons.Add(weapon);
        }
        return weapon;
    }
}
