﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public abstract class Projectile : MonoBehaviour
{
    // members //////////////////////////////////////////////////////////////////
    public Vector3 Direction;
    public float ProjectileSpeed = 5.0f;
    public float LifeTime = 2.0f;
    public float Damage = 0.4f;
    public float RotationSpeed = 5f;

    private bool isShot = false;

    [HideInInspector]
    public Weapon WeaponShotWith;
    public Rigidbody rb { get; private set; }
    string ownerTag = "";


	// Use this for initialization
	protected virtual void Awake ()
    {
        rb = GetComponent<Rigidbody>();
	}

    void OnTriggerEnter(Collider other)
    {
        if((WeaponShotWith == null || WeaponShotWith.owner == null) && ownerTag == "")
        {
            Destroy(gameObject);
        }
        if(ownerTag == "")
        {
            ownerTag = WeaponShotWith.owner.tag;
        }

        if (other.gameObject.tag != ownerTag && other.gameObject.tag != "Projectile"
            && other.gameObject.tag != GameJammers.Infection.EnvironmentTags.WakeUpArea )
        {
            DidHitObject(other.gameObject);

            if(other.gameObject.GetComponent<Enemy>() != null)
            {
                Enemy hitEnemy = other.gameObject.GetComponent<Enemy>();
                if(hitEnemy)
                {
                    hitEnemy.WasHitByProjectile(Damage);
                }
            }
            if(other.gameObject.tag == "Player")
            {
                Player player = other.gameObject.GetComponentInChildren<Player>();
                if(player)
                {
                    player.TakeDamage(Damage);
                }
            }

            Destroy(gameObject);
        }
    }

    public void Fire(Vector3 direction)
    {
        Direction = direction;
        isShot = true;
    }

	// Update is called once per frame
    //Dont forget to add base.Update() in child class Update()
	protected virtual void Update ()
    {
        if(isShot)
        {
            MoveInDirection();
            LifeTime -= Time.deltaTime;
            if(LifeTime <= 0)
            {
                Destroy(gameObject);
            }
        }
	}

    private void MoveInDirection()
    {
        rb.velocity = Direction * ProjectileSpeed;
        transform.Rotate(Vector3.right, RotationSpeed * Time.deltaTime);
    }

    public abstract void DidHitObject(GameObject hitObject);
}
