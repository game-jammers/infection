//
// Game Jammers 2016
//

using blacktriangles;
using UnityEngine;
using UnityEngine.UI;

namespace GameJammers.Infection
{
    class AbilityItem
        : UIListElement
    {
        // members /////////////////////////////////////////////////////////////
        public UseableSkill skill                           { get { return data as UseableSkill; } }
        public Image image                                  = null;
        public Image background                             = null;

        // public methods //////////////////////////////////////////////////////
        public override void Refresh()
        {
            base.Refresh();
            if( skill == null ) return;
            image.sprite = Resources.Load<Sprite>( skill.Icon );
            background.sprite = image.sprite;
            image.fillAmount = skill.progress;
        }
    }
}
