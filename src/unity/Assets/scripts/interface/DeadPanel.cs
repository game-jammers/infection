//
// Game Jammers 2016
//

using blacktriangles;
using UnityEngine;
using UnityEngine.UI;

namespace GameJammers.Infection
{
    public class DeadPanel
        : UIElement
    {
        // members /////////////////////////////////////////////////////////////
        public GameSceneController sceneController          { get; private set; }
        public Text tip                                     = null;

        [SerializeField] string[] tips = {
                "Abilities use ATP, which regenerates over time",
                "Collect DNA from destroyed enemies to evolve new skills and abilities"
            };


        // public methods //////////////////////////////////////////////////////

        // unity callbacks /////////////////////////////////////////////////////
        protected virtual void Start()
        {
            tip.text = tips.Random();
        }
    }
}
