//
// Game Jammers 2016
//

using blacktriangles;
using UnityEngine;
using UnityEngine.UI;

namespace GameJammers.Infection
{
    public class GamePanel
        : UIElement
    {
        // members /////////////////////////////////////////////////////////////
        public GameSceneController sceneController          { get; private set; }
        public Player player                                { get { return sceneController.player; } }
        public Slider healthBar                             = null;
        public Slider atpBar                                = null;
        public UIList abilities                             = null;

        // public methods //////////////////////////////////////////////////////

        // unity callbacks /////////////////////////////////////////////////////
        protected virtual void Start()
        {
            sceneController = SceneController.instance as GameSceneController;
        }

        protected virtual void Update()
        {
            if( sceneController == null || player == null )
            {
                return;
            }

            healthBar.value = player.Health;
            atpBar.value = player.ATP;
            abilities.SetData( player.Skills );
        }
    }
}
