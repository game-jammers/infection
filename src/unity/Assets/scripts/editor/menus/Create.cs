//
// Game Jammers 2016
//

﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

using blacktriangles;

namespace GameJammers.Infection
{
	public static class CreateMenu
	{
		[MenuItem( "Assets/Create/Infection/Tileset" )]
		public static void CreateTileset()
		{
			Create.Asset<Tileset>();
		}
	}
}
