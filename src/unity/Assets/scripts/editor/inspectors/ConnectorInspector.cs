//
// Game Jammers 2016
//

using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using blacktriangles;

namespace GameJammers.Infection
{
    [CustomEditor( typeof( Connector ) )]
    public class ConnectorInspector
        : Editor
    {
        // constants //////////////////////////////////////////////////////////
        private const float kConeSize                           = 0.5f;

		// members ////////////////////////////////////////////////////////////
		Connector connector										{ get { return target as Connector; } }

        // unity callbacks ////////////////////////////////////////////////////
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            GUILayout.BeginVertical( GUI.skin.box );
                Connector conn = connector;
                EditorGUILayout.Toggle( "Is Initialized", conn.owner != null );
                EditorGUILayout.Toggle( "Is Connected", conn.isConnected );
            GUILayout.EndVertical();

            GUILayout.BeginVertical( GUI.skin.box );
                EditorGUILayout.ObjectField( "Connected To", conn.connectedTo, typeof( Connector ), false );
            GUILayout.EndVertical();
        }

		protected virtual void OnSceneGUI()
		{
			Handles.color = Color.red;
            Vector3 coneOffset = connector.transform.forward*kConeSize/2f + new Vector3( 0f, connector.size/2f, 0f );
			Handles.ConeCap( 0, connector.transform.position + coneOffset, connector.transform.rotation, kConeSize );

            Handles.color = new Color( 1f, 0f, 0f, 0.25f );
            Vector3[] verts = connector.GetPortalVertices();
            Handles.DrawSolidRectangleWithOutline( verts, Color.gray, Color.red );
		}
    }
}
