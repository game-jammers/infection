//
// Game Jammers 2016
//

using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using blacktriangles;

namespace GameJammers.Infection
{
    [CustomEditor( typeof( Room ) )]
    public class RoomInspector
        : Editor
    {
        // constants //////////////////////////////////////////////////////////

		// members ////////////////////////////////////////////////////////////
		private Room room									    { get { return target as Room; } }

        // public methods /////////////////////////////////////////////////////
        public static void CollectColliders( Room curRoom )
        {
            curRoom.colliders = System.Array.FindAll( curRoom.transform.GetComponentsInChildren<Collider>(), (col)=>{
                return col.gameObject.tag == EnvironmentTags.RoomFootprint;
            });
        }

        public static void CollectConnectors( Room curRoom )
        {
            List<Connector> connectors = new List<Connector>();
            if( curRoom != null )
            {
                foreach( Transform child in curRoom.transform )
                {
                    CollectConnectors( child, connectors );
                }

                curRoom.connectors = connectors.ToArray();
            }
        }


        private static void CollectSpawns( Room curRoom )
        {
            curRoom.playerSpawn = new List<Transform>( System.Array.FindAll( curRoom.transform.GetComponentsInChildren<Transform>(), (trans)=>{
                return trans.gameObject.tag == EnvironmentTags.PlayerSpawn;
            }));

            curRoom.enemySpawn = new List<Transform>( System.Array.FindAll( curRoom.transform.GetComponentsInChildren<Transform>(), (trans)=>{
                return trans.gameObject.tag == EnvironmentTags.EnemySpawn;
            }));
        }

        // unity callbacks ////////////////////////////////////////////////////
        public override void OnInspectorGUI()
        {
            Room curRoom = room;
            if( curRoom == null ) return;

            blacktriangles.EditorGUIUtility.OnGUI();

             GUILayout.BeginVertical( GUIStyles.darkbox );
                 GUILayout.Label( "Spawn Points", GUIStyles.buttonHighlight );

                blacktriangles.EditorGUIUtility.QuickListField<Transform>(
                        "Player Spawns",
                        curRoom.playerSpawn,
                        (item)=>{ return blacktriangles.EditorGUIUtility.ThinObjectField( "", item, true); },
                        blacktriangles.EditorGUIUtility.GetNullClass<Transform>,
                        GUIStyles.buttonNormal
                    );

                 blacktriangles.EditorGUIUtility.QuickListField<Transform>(
                         "Enemy Spawns",
                         curRoom.enemySpawn,
                         (item)=>{ return blacktriangles.EditorGUIUtility.ThinObjectField( "", item, true); },
                         blacktriangles.EditorGUIUtility.GetNullClass<Transform>,
                         GUIStyles.buttonNormal
                     );

                 if( GUILayout.Button( "Update Spawns", GUIStyles.lightButtonNormal ) )
                 {
                     CollectSpawns( curRoom );
                 }
             GUILayout.EndVertical();

            GUILayout.BeginVertical( GUIStyles.darkbox );
                GUILayout.Label( "Connectors", GUIStyles.buttonHighlight );

                if( curRoom.connectors != null )
                {
                    foreach( Connector connector in curRoom.connectors )
                    {
                        EditorGUILayout.ObjectField( connector, typeof( Connector ), true );
                    }
                }

                if( GUILayout.Button( "Update Connectors", GUIStyles.lightButtonNormal ) )
                {
                    CollectConnectors( curRoom );
                }
            GUILayout.EndVertical();

            GUILayout.BeginVertical( GUIStyles.darkbox );
                GUILayout.Label( "Colliders", GUIStyles.buttonHighlight );

                if( curRoom.colliders != null )
                {
                    foreach( Collider collider in curRoom.colliders )
                    {
                        EditorGUILayout.ObjectField( collider, typeof( Collider ), true );
                    }
                }

                if( GUILayout.Button( "Update Colliders", GUIStyles.lightButtonNormal ) )
                {
                    CollectColliders( curRoom );
                }
            GUILayout.EndVertical();
        }

        // private methods ////////////////////////////////////////////////////
        private static void CollectConnectors( Transform transform, List<Connector> connectors )
        {
            Connector connector = transform.GetComponent<Connector>();
            if( connector != null )
            {
                connectors.Add( connector );
            }

            foreach( Transform child in transform )
            {
                CollectConnectors( child, connectors );
            }
        }
    }
}
