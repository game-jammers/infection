﻿using UnityEngine;
using System.Collections;

public class Moveable
    : MonoBehaviour
{
    // members /////////////////////////////////////////////////////////////////
    public float MovementSpeed = 5f;
    public float RotationSpeed = 10f;

    public Rigidbody rb = null;
    private Vector3 velocity = Vector3.zero;

    // public methods //////////////////////////////////////////////////////////
    public void RotateTo(Quaternion targetRotation)
    {
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, RotationSpeed * Time.deltaTime);
    }

    public void LookAt(Vector3 target)
    {
        // Determine the target rotation.  This is the rotation if the transform looks at the target point.
        Quaternion targetRotation = Quaternion.LookRotation(target - transform.position);
        RotateTo(targetRotation);
    }

    public void RotateBy(float rotation)
    {
        transform.Rotate(0, rotation * RotationSpeed, 0);
    }

    public void MoveInDirection(float vertical, float horizontal)
    {
        velocity += new Vector3( horizontal, 0.0f, vertical ) * MovementSpeed;
    }

    public void MoveInDirection(Vector3 direction)
    {
        velocity += direction * MovementSpeed;
    }

	// unity callbacks /////////////////////////////////////////////////////////
	protected virtual void Awake ()
    {
        rb = GetComponentInParent<Rigidbody>();
        if( rb == null ) throw new System.NullReferenceException( "No Rigidbody" );
    }

    protected virtual void Start()
    {
        // just make it virtual at the top of the chain to be consistent
    }

    protected virtual void Update()
    {
        rb.velocity = velocity;
        velocity = Vector3.zero;
    }
}
