//
// Game Jammers 2016
//

using blacktriangles;
﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace GameJammers.Infection
{
	public class DungeonGenerator
		: MonoBehaviour
	{
		// types //////////////////////////////////////////////////////////////
		public struct CompleteMessage
		{
			public Dungeon dungeon;
			public GameObject root;
		}

		public delegate void DungeonGenCompleteCallback( CompleteMessage message );

		// members ////////////////////////////////////////////////////////////
		public Tileset tileset									= null;
		public int maxDepth										= 10;
		public float maxOffsetY									= 0f;

		public bool isWorking									{ get; private set; }
		public GameObject root									{ get; private set; }
		public Dungeon dungeon									{ get; private set; }

		private Queue<Room> procRooms							= null;
		[SerializeField] Room bossRoom						= null;

		// public methods /////////////////////////////////////////////////////
		public void Build()
		{
			Build( null );
		}

		public void Build( DungeonGenCompleteCallback callback )
		{
			if( tileset == null )
			{
				callback( new CompleteMessage() );
			}
			else
			{
				StartCoroutine( CreateDungeonStep( callback ) );
			}
		}

		// private methods ////////////////////////////////////////////////////
		private void Reset()
		{
			isWorking = false;
			if( root != null )
			{
				GameObject.Destroy( root );
			}

			root = new GameObject( "Root" );
			root.transform.parent = transform;

			procRooms = new Queue<Room>();
			dungeon = new Dungeon();
		}

		private IEnumerator CreateDungeonStep( DungeonGenCompleteCallback callback )
		{
			// reset the generator to starting defaults
			Reset();

			isWorking = true;

			// create the first room
			Room startRoom = Tileset.CreateRoom( bossRoom );
            startRoom.gameObject.SetActive(true);
			startRoom.transform.parent = root.transform;

			AddRoom( startRoom );

			int currentDepth = 0;
			while( procRooms.Count > 0 )
			{
				if( currentDepth > maxDepth )
				{
					break;
				}

				Room currentRoom = procRooms.Dequeue();

				Connector[] connectors = currentRoom.connectors.ShallowClone();
				connectors.Shuffle();
				foreach( Connector connector in connectors )
				{
					// skip it if it's already connected
					if( connector.isConnected )
					{
						continue;
					}

					// try every available room to see if it can connect
					Room[] prefabs = tileset.GetRandomizedRooms();
					foreach( Room prefab in prefabs )
					{
						Room tryRoom = Tileset.CreateRoom( prefab );

						if( TryConnect( connector, tryRoom ) )
						{
							AddRoom( tryRoom );
							break;
						}
						else
						{
							Destroy( tryRoom.gameObject );
						}
					}
				}

				++currentDepth;
				yield return new WaitForSeconds( 0f );
			}

			dungeon.Refresh();

			// notify via callback that generation is complete
			CompleteMessage msg = new CompleteMessage();
			msg.dungeon = dungeon;
			msg.root = root;

			yield return new WaitForSeconds( 3.0f );
			if( callback != null )
			{
				callback( msg );
			}

			isWorking = false;
		}

		private void AddRoom( Room room )
		{
			room.transform.parent = root.transform;
			room.transform.position += new Vector3( 0f, btRandom.Range( -maxOffsetY, maxOffsetY ), 0f );
			room.Populate();
			dungeon.AddRoom( room );
			procRooms.Enqueue( room );
		}

		private bool TryConnect( Connector target, Room tryRoom )
		{
			bool result = false;

			float targetRot = ( target.transform.rotation.eulerAngles.y + 180f ) % 360f;

			foreach( Connector tryConnector in tryRoom.connectors )
			{
				// only same sized connectors are compatible
				if( target.size != tryConnector.size )
					continue;

				float currentRot = tryConnector.transform.rotation.eulerAngles.y;
				float deltaRot = targetRot - currentRot;

				tryConnector.owner.transform.RotateAround( tryConnector.transform.position, Vector3.up, deltaRot );

				Vector3 fromForward = Vector3.zero;
				Vector3 positionOffset = tryConnector.owner.transform.TransformDirection( tryConnector.transform.localPosition );
				tryConnector.owner.transform.position = target.transform.position - positionOffset + fromForward;

				if( !tryRoom.IsOverlapping( dungeon.rooms ) )
				{
					// connect them!
					tryConnector.Connect( target );
					result = true;
					break;
				}
			}

			return result;
		}
	}
}
