//
// Game Jammers 2016
//

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using blacktriangles;

namespace GameJammers.Infection
{
	public class Dungeon
	{
		// members ////////////////////////////////////////////////////////////
		private List<Room> _rooms							= new List<Room>();
		public IList<Room> rooms							{ get { return _rooms.AsReadOnly(); } }
		public Bounds bounds								{ get { return _bounds; } }
		private Bounds _bounds								= default(Bounds);

		// constructor / initializer //////////////////////////////////////////
		public Dungeon()
		{
		}

		// public static methods //////////////////////////////////////////////
		public static bool ScreenToGroundPos( Vector3 screenPos, out RaycastHit hit )
		{
			btCamera sceneCam = SceneController.instance.sceneCam;
			Vector3 center = new Vector3( Screen.width / 2f, Screen.height / 2f, 0f );
			Ray ray = sceneCam.ScreenToRay( center );

			return Physics.Raycast( ray.origin, ray.direction, out hit, Mathf.Infinity, EnvironmentLayers.Walkable );
		}

		// public methods /////////////////////////////////////////////////////
		public void AddRoom( Room room )
		{
			_rooms.Add( room );
			_bounds.Encapsulate( room.bounds );
			room.OnAdded( this );
		}

		public Room GetRandomRoom()
		{
			return _rooms.Random();
		}

		public Transform GetRandomPlayerSpawn()
		{
			Transform result = null;
			while( result == null )
			{
				Room room = GetRandomRoom();
				result = room.playerSpawn.Random();
			}
			return result;
		}

		public void Refresh()
		{
			foreach( Room room in rooms )
			{
				room.Refresh();
			}
		}
	}
}
