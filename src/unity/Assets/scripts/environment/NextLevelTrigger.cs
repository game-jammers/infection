//
// Game Jammers 2016
//

using UnityEngine;

namespace GameJammers.Infection
{
    public class NextLevelTrigger
        : MonoBehaviour
    {
        // unity callbacks /////////////////////////////////////////////////////
        protected virtual void OnTriggerEnter(Collider other)
        {
            if( (other.gameObject.layer & EnvironmentLayers.Player) != 0 )
            {
                GameSceneController sceneCont = SceneController.instance as GameSceneController;
                sceneCont.NextLevel();
            }
        }
    }
}
