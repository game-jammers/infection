//
// Game Jammers 2016
//

using UnityEngine;
using blacktriangles;

namespace GameJammers.Infection
{
    public class Throb
        : MonoBehaviour
    {
        // members /////////////////////////////////////////////////////////////
        public Vector2 throbX                                   = new Vector2(0.8f, 1.0f);
        public Vector2 throbY                                   = new Vector2(0.8f, 1.0f);

        public float speed                                      = 1f;
        public Vector2 offset                                   = new Vector2(0f, 1f);

        private System.Random rnd;
        private float offsetTime;

        // unity callbacks /////////////////////////////////////////////////////
        protected virtual void Start()
        {
            rnd = new System.Random();
            offsetTime = (float)rnd.NextDouble(offset.x, offset.y);
        }

        protected virtual void Update()
        {
            float time = (float)Time.time;
            float x = throbX.x + (throbX.y-throbX.x) * Mathf.Abs( Mathf.Sin( time * speed + offsetTime ) );
            float z = throbY.y + (throbY.y-throbY.x) * Mathf.Abs( Mathf.Sin( time * speed + offsetTime ) );
            transform.localScale = new Vector3( x, 1.0f, z );
        }
    }
}
