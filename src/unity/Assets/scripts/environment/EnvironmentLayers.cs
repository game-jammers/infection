//
// Game Jammers 2016
//

﻿using UnityEngine;

namespace GameJammers.Infection
{
	public static class EnvironmentLayers
	{
		// contants ///////////////////////////////////////////////////////////
		public static readonly string kPlayerLayerName		= "Player";
		public static readonly string kEnemyLayerName		= "Enemy";
		public static readonly string kWalkableLayerName	= "Walkable";

		public static readonly LayerMask Player				= LayerMask.GetMask( kPlayerLayerName );
		public static readonly LayerMask Enemy				= LayerMask.GetMask( kEnemyLayerName );
		public static readonly LayerMask Walkable			= LayerMask.GetMask( kWalkableLayerName );
	}

	public static class EnvironmentTags
	{
		public static readonly string Player				= "Player";
		public static readonly string RoomFootprint			= "RoomFootprint";
		public static readonly string PlayerSpawn			= "PlayerSpawn";
		public static readonly string EnemySpawn			= "EnemySpawn";
		public static readonly string WakeUpArea			= "WakeUpArea";
	}
}
