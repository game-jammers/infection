//
// Game Jammers 2016
//

﻿using UnityEngine;
using System.Collections;
using blacktriangles;

namespace GameJammers.Infection
{
	public class Tileset
		: ScriptableObject
	{
		// members ////////////////////////////////////////////////////////////
		public Room[] rooms										= null;

		// public methods /////////////////////////////////////////////////////
		public Room CreateRandomRoom()
		{
			int rndIndex = Random.Range( 0, rooms.Length );
			return CreateRoom( rndIndex );
		}

		public Room CreateRoom( int index )
		{
			if( rooms.IsValidIndex( index ) )
				return CreateRoom( rooms[index] );

			return null;
		}

		public static Room CreateRoom( Room prefab )
		{
			return GameObject.Instantiate( prefab, Vector3.zero, Quaternion.identity ) as Room;
		}

		public Room[] GetRandomizedRooms()
		{
			Room[] result = rooms.Clone() as Room[];
			result.Shuffle();
			return result;
		}
	}
}
