//
// Game Jammers 2016
//

using blacktriangles;
using UnityEngine;
using System.Collections;

namespace GameJammers.Infection
{
    public class MusicTrigger
        : MonoBehaviour
    {
        // members /////////////////////////////////////////////////////////////
        public AudioClip enterMusic;
        public AudioClip exitMusic;

        // unity callbacks /////////////////////////////////////////////////////
         void OnTriggerEnter(Collider other)
         {

              if( other.gameObject.tag == EnvironmentTags.Player )
             {
                GameSceneController sceneCont = SceneController.instance as GameSceneController;
                sceneCont.musicPlayer.clip = enterMusic;
                sceneCont.musicPlayer.PlayDelayed(0.0f);
             }
         }

         void OnTriggerExit(Collider other)
         {
              if( other.gameObject.tag == EnvironmentTags.Player )
             {
                 GameSceneController sceneCont = SceneController.instance as GameSceneController;
                sceneCont.musicPlayer.clip = exitMusic;
                sceneCont.musicPlayer.PlayDelayed(0.0f);
             }
         }
    }
}
