//
// Game Jammers 2016
//

﻿using UnityEngine;
using System.Collections.Generic;

namespace GameJammers.Infection
{
	public class Connector
		: MonoBehaviour
	{
		// members ////////////////////////////////////////////////////////////
		public Room owner										{ get; private set; }
		public Room connectedRoom								{ get { return connectedTo == null ? null : connectedTo.owner; } }
		public Connector connectedTo							{ get; private set; }
		public bool isConnected									{ get { return connectedTo != null; } }
		public int size											= 2;

		public GameObject connectedGo							= null;
		public GameObject sealedGo								= null;

		// constructor / initializer //////////////////////////////////////////
		public void Initialize( Room _owner )
		{
			owner = _owner;
		}

		// public methods /////////////////////////////////////////////////////
		public Vector3[] GetPortalVertices()
		{
			Vector3[] result = new Vector3[ 4 ];

			float offset = size / 2f;

			Vector3 pos = transform.position;
			result[0] = pos + transform.TransformDirection( new Vector3( -offset, 0f, 0f ) );
			result[1] = pos + transform.TransformDirection( new Vector3( -offset, offset*2f, 0f ) );
			result[2] = pos + transform.TransformDirection( new Vector3( offset, offset*2f, 0f ) );
			result[3] = pos + transform.TransformDirection( new Vector3( offset, 0f, 0f ) );

			return result;
		}

		public void Connect( Connector target )
		{
			ConnectOneWay( target );
			target.ConnectOneWay( this );
		}

		public void OnAdded( Dungeon dungeon )
		{
			if( isConnected ) return;

			foreach( Room room in dungeon.rooms )
			{
				foreach( Connector conn in room.connectors )
				{
					if( conn != this && !conn.isConnected && Vector3.Distance( transform.position, conn.transform.position ) < 0.25f )
					{
						Connect( conn );
					}
				}
			}
		}

		public void Refresh()
		{
			if( connectedGo != null )
				connectedGo.SetActive( isConnected );

			if( sealedGo != null )
				sealedGo.SetActive( !isConnected );
		}

		// private methods ////////////////////////////////////////////////////
		private void ConnectOneWay( Connector target )
		{
			connectedTo = target;
		}

		// unity callbacks ////////////////////////////////////////////////////
		protected virtual void OnDrawGizmosSelected()
		{
			Gizmos.color = isConnected ? Color.green : Color.red;
			Gizmos.DrawSphere( transform.position - (transform.forward*0.25f), 0.25f );
		}
	}
}
