//
// Game Jammers 2016
//

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using blacktriangles;

namespace GameJammers.Infection
{
	public class Room
		: MonoBehaviour
	{
		// members ////////////////////////////////////////////////////////////
		public Connector[] connectors						= null;
		public Collider[] colliders							= null;
		public Bounds bounds								{ get { return GetBounds(); } }
		public List<Transform> playerSpawn					= new List<Transform>();
		public Vector2Int enemyCount					    = new Vector2Int( 5, 20 );
		public List<Transform> enemySpawn					= new List<Transform>();
		private bool areBoundsDirty							= true;
		private Bounds _bounds								= default(Bounds);

		// public methods /////////////////////////////////////////////////////
		public bool IsOverlapping( IEnumerable otherRooms )
		{
			foreach( Collider collider in colliders )
			{
				foreach( Room otherRoom in otherRooms )
				{
					foreach( Collider otherCollider in otherRoom.colliders )
					{
						if( collider.bounds.Intersects( otherCollider.bounds ) )
							return true;
					}
				}
			}

			return false;
		}

		public void Refresh()
		{
			foreach( Connector conn in connectors )
			{
				conn.Refresh();
			}
		}

		public void Populate()
		{
			PopulateEnemies();
			PopulateProps();
		}

		// callbacks //////////////////////////////////////////////////////////
		public void OnAdded( Dungeon dungeon )
		{
			foreach( Connector conn in connectors )
			{
				conn.OnAdded( dungeon );
			}
		}

		// private methods ////////////////////////////////////////////////////
		private Bounds GetBounds()
		{
			if( areBoundsDirty )
			{
				foreach( Collider collider in colliders )
				{
					_bounds.Encapsulate( collider.bounds );
				}
			}

			return _bounds;
		}

		private void PopulateEnemies()
		{
			GameSceneController sceneCont = SceneController.instance as GameSceneController;
			if( sceneCont == null ) return;
			if( enemySpawn.Count <= 0 ) return;

            System.Random rnd = new System.Random();
			int targetCount = rnd.Next(enemyCount.x, enemyCount.y);
			for( int i = 0; i < targetCount; ++i )
			{
				Transform point = enemySpawn.Random();
				GameObject enemy = Instantiate( sceneCont.enemyPrefabs.Random() );
				enemy.transform.SetParent( transform, true );
				enemy.transform.position = point.position;
			}
		}

		private void PopulateProps()
		{
		}

		// unity callbacks ////////////////////////////////////////////////////
		protected virtual void Awake()
		{
			foreach( Connector conn in connectors )
			{
				conn.Initialize( this );
			}
		}
	}
}
