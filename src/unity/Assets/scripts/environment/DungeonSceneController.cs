//
// Game Jammers 2016
//

﻿using UnityEngine;
using System.Collections;

namespace GameJammers.Infection
{
	public class DungeonSceneController
		: SceneController
	{
		// types //////////////////////////////////////////////////////////////
		public enum State { Initializing, BuildingGeometry, PostProcessing, Ready };

		// members ////////////////////////////////////////////////////////////
		[SerializeField] DungeonGenerator generator				= null;

		public State state										{ get; private set; }
		public Dungeon dungeon									{ get; private set; }
		public bool isWorking									{ get { return state != State.Initializing && state != State.Ready; } }

		// public methods /////////////////////////////////////////////////////
		protected virtual void BuildDungeon( System.Action callback )
		{
			if( isWorking ) return;

			state = State.BuildingGeometry;
			generator.Build( (msg)=>{
				dungeon = msg.dungeon;
				state = State.PostProcessing;
				StartCoroutine( PostProcessDungeon( ()=>{
					state = State.Ready;
					callback();
				}));
			});
		}


		protected virtual IEnumerator PostProcessDungeon( System.Action completeCallback )
		{
			// most population happens as a part of generation, but you can add custom
			// rules here for
			yield return new WaitForSeconds( 0f );
			completeCallback();
		}
	}
}
