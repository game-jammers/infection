//
// Game Jammers 2016
//

using blacktriangles;
using UnityEngine;
using UnityEngine.UI;

using System.Collections;

namespace GameJammers.Infection
{
    public class GameManager
        : BaseGameManager
    {
        // constants //////////////////////////////////////////////////////////////
        public static readonly string kPrefabPath                   = "managers/GameManager";

        // accessors //////////////////////////////////////////////////////////////
        public static new GameManager instance                      { get; private set; }

        // public methods /////////////////////////////////////////////////////////
        public static GameManager EnsureExists()
        {
        	if( instance == null )
            {
                instance = EnsureExists<GameManager>( kPrefabPath );
                instance.name = "GameManager";
            }

            return instance;
        }

        public void DelayCall( float time, System.Action action )
        {
            StartCoroutine( DelayCallInternal( time, action ) );
        }

        // private methods ////////////////////////////////////////////////////////
        private IEnumerator DelayCallInternal( float time, System.Action action )
        {
            yield return new WaitForSeconds( time );
            action();
        }
    };
}
