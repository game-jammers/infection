//
// Game Jammers 2016
//

using UnityEngine;
using UnityEngine.UI;
using blacktriangles;

namespace GameJammers.Infection
{
    public class SceneManager
        : BaseSceneManager
    {
        // constants //////////////////////////////////////////////////////////////
        public static readonly string kPrefabPath                   = "managers/SceneManager";

        // accessors //////////////////////////////////////////////////////////////
        public static new SceneManager instance                     { get; private set; }

        // public methods /////////////////////////////////////////////////////////
        public static SceneManager EnsureExists()
        {
            if( instance == null )
            {
                instance = EnsureExists<SceneManager>( kPrefabPath );
                instance.name = "SceneManager";
            }

            return instance;
        }
    };
}
