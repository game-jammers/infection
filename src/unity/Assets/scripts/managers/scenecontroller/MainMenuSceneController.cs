//
// Game Jammers 2016
//

using blacktriangles;
using blacktriangles.Input;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace GameJammers.Infection
{
	public class MainMenuSceneController
		: SceneController
	{
		// members /////////////////////////////////////////////////////////////
		[SerializeField] GameObject[] cast					= null;
		[SerializeField] Transform pointA					= null;
		[SerializeField] Transform pointB					= null;
		[SerializeField] Transform meetA					= null;
		[SerializeField] Transform meetB					= null;

		public GameObject currentCharacterA					= null;
		public GameObject currentCharacterB					= null;

        // public methods //////////////////////////////////////////////////////
		public void StartGame()
		{
			UnityEngine.SceneManagement.SceneManager.LoadScene( 1 );
		}

		// unity callbacks /////////////////////////////////////////////////////
		protected virtual void Start()
		{
			PlayRandomVignette();
		}

		// private methods /////////////////////////////////////////////////////
		private void PlayRandomVignette()
		{
			Cleanup();

			System.Func<IEnumerator>[] vignettes = {
					WalkRight,
					WalkLeft,
					BumpAndRun
				};

			StartCoroutine( vignettes.Random()() );
		}

		private void Cleanup()
		{
			Destroy( currentCharacterA );
			Destroy( currentCharacterB );
		}

		private GameObject SpawnActor( GameObject prefab, Transform spawnPoint )
		{
			return Instantiate( cast.Random(), spawnPoint.position, spawnPoint.rotation ) as GameObject;
		}

		private void WalkToPoint( GameObject actor, Transform target, float speed, System.Action finished )
		{
			StartCoroutine( DoWalk( actor, target, speed, finished ) );
		}

		private IEnumerator DoWalk( GameObject actor, Transform target, float speed, System.Action finished )
		{
			float dist = ( actor.transform.position - target.position ).magnitude;
			while( dist > 1 )
			{
				actor.transform.LookAt( target );
				Vector3 diff = target.position - actor.transform.position;
				actor.transform.position += diff.normalized * speed * Time.deltaTime;
				dist = diff.magnitude;
				yield return new WaitForSeconds( 0.0f );
			}

			finished();
		}

		private void Bounce( GameObject actor, float time, float height, System.Action finished )
		{
			StartCoroutine( DoBounce( actor, time, height, finished ) );
		}

		private IEnumerator DoBounce( GameObject actor, float time, float height, System.Action finished )
		{
			float start = actor.transform.position.y;

			float elapsed = 0f;
			while( elapsed < time )
			{
				elapsed += Time.deltaTime;
				float perc = elapsed / time;
				actor.transform.position = new Vector3( actor.transform.position.x, (start + height)*perc, actor.transform.position.z );
				yield return new WaitForSeconds(0f);
			}

			elapsed = 0f;
			while( elapsed < time )
			{
				elapsed += Time.deltaTime;
				float perc = 1.0f - (elapsed / time);
				actor.transform.position = new Vector3( actor.transform.position.x, (start + height)*perc, actor.transform.position.z );
				yield return new WaitForSeconds(0f);
			}

			finished();
		}

		// cutscenes ///////////////////////////////////////////////////////////
		private IEnumerator WalkRight()
		{
			currentCharacterA = SpawnActor( cast.Random(), pointA );

			bool walking = true;
			WalkToPoint( currentCharacterA, pointB, 2.0f, ()=>{walking=false;} );
			while( walking )
			{
				yield return new WaitForSeconds( 0.0f );
			}

			yield return new WaitForSeconds( 1.0f );

			PlayRandomVignette();

		}

		private IEnumerator WalkLeft()
		{
			currentCharacterA = SpawnActor( cast.Random(), pointB );

			bool walking = true;
			WalkToPoint( currentCharacterA, pointA, 2.0f, ()=>{walking=false;} );
			while( walking )
			{
				yield return new WaitForSeconds( 0.0f );
			}

			yield return new WaitForSeconds( 1.0f );

			PlayRandomVignette();
		}

		private IEnumerator BumpAndRun()
		{
			currentCharacterA = SpawnActor( cast.Random(), pointA );
			currentCharacterB = SpawnActor( cast.Random(), pointB );

			bool walkingA = true;
			bool walkingB = true;
			WalkToPoint( currentCharacterA, meetA, 2.0f, ()=>{walkingA=false;} );
			WalkToPoint( currentCharacterB, meetB, 2.0f, ()=>{walkingB=false;} );
			while( walkingA || walkingB )
			{
				yield return new WaitForSeconds( 0.0f );
			}

			walkingA = true;
			walkingB = true;
			Bounce( currentCharacterA, 0.25f, 1.0f, ()=>{walkingA=false;} );
			Bounce( currentCharacterB, 0.25f, 1.0f, ()=>{walkingB=false;} );
			while( walkingA || walkingB )
			{
				yield return new WaitForSeconds( 0.0f );
			}

			walkingA = true;
			walkingB = true;
			WalkToPoint( currentCharacterA, pointA, 5.0f, ()=>{walkingA=false;} );
			WalkToPoint( currentCharacterB, pointB, 5.0f, ()=>{walkingB=false;} );
			while( walkingA || walkingB )
			{
				yield return new WaitForSeconds( 0.0f );
			}

			yield return new WaitForSeconds( 1.0f );

			PlayRandomVignette();

		}
	};
}
