//
// Game Jammers 2016
//

using blacktriangles;
using blacktriangles.Input;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace GameJammers.Infection
{
	public class GameSceneController
		: DungeonSceneController
	{
		// members /////////////////////////////////////////////////////////////
		public Player player								{ get; private set; }
		public bool ready									= false;
		public GamePanel gamePanel							= null;
		public UIElement deadPanel							= null;
		public UIElement nextLevel							= null;
		[SerializeField] GameObject playerPrefab			= null;

		public GameObject[] enemyPrefabs					= null;
		public AudioSource musicPlayer						= null;

		private bool dead									= false;

		// public methods //////////////////////////////////////////////////////
		public void Restart()
		{
			int scene = UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex;
         	UnityEngine.SceneManagement.SceneManager.LoadScene(scene, UnityEngine.SceneManagement.LoadSceneMode.Single);
		}

		public void FinishLevel()
		{
			// right now nothing special to do for next level
			player.Godmode = true;
			nextLevel.Show();
		}

		public void NextLevel()
		{
			Restart();
		}

		// unity callbacks /////////////////////////////////////////////////////
		protected virtual void Start()
		{
			nextLevel.Hide(0.0f);
			deadPanel.Hide(0.0f);
			BuildDungeon( ()=>{
				sceneCam.gameObject.SetActive( false );
				GameObject playerGo = Instantiate( playerPrefab );
				playerGo.transform.position = dungeon.GetRandomPlayerSpawn().position;
				playerGo.transform.position = new Vector3( playerGo.transform.position.x, -1.5f, playerGo.transform.position.z );
				player = playerGo.GetComponentInChildren<Player>();
				gamePanel.Show();
				ready = true;
			});
		}

		protected virtual void Update()
		{
			if( player == null ) return;
			if( player.Alive == false && !dead )
			{
				dead = true;
				gamePanel.Hide();
				deadPanel.Show();
			}
		}
	};
}
