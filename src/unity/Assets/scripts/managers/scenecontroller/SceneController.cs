//
// Game Jammers 2016
//

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using blacktriangles;
using blacktriangles.Input;

namespace GameJammers.Infection
{
	public class SceneController
		: BaseSceneController
	{
    // members ////////////////////////////////////////////////////////////////
    public static SceneController instance                      { get; private set; }

    // public methods /////////////////////////////////////////////////////////
    public static T GetInstance<T>()
		where T: SceneController
    {
		return instance as T;
    }

    // unity callbacks ////////////////////////////////////////////////////////
    protected virtual void Awake()
    {
		instance = this;
		GameManager.EnsureExists();
		SceneManager.EnsureExists();
		SceneManager.instance.OnSceneControllerLoaded( instance );
    }
  };
}
