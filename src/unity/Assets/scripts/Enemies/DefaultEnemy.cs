﻿using UnityEngine;
using System.Collections;
using System;

public class DefaultEnemy
    : Enemy
{
    // members /////////////////////////////////////////////////////////////////
    float speed = 4.0f;

    float maxRightMovement = 0.0f;
    float minRightMovement = 0.0f;
    float maxTopMovement = 0.0f;
    float minTopMovement = 0.0f;
    float curRightMovement = 0.0f;
    float curTopMovement = 0.0f;

    float curNewMoveCooldown = 0.0f;
    float newMoveCooldownMax = 2.0f;
    float newMoveCooldownMin = 1.2f;

    float rotationMax = 180f;
    float rotationMin = -180f;
    float curRotataion = 0.0f;

    public bool MoveInFacingDirection = false;

    public bool MoveTowardsPlayer = false;

    // unity callbacks /////////////////////////////////////////////////////////
    protected override void Awake()
    {
        base.Awake();
    }

    // Use this for initialization
    protected override void Start () {
        base.Start();
        MovementSpeed = 0.2f;
        RotationSpeed = 2.0f;

        maxRightMovement = speed;
        minRightMovement = -speed;
        maxTopMovement = speed;
        minTopMovement = -speed;
    }

	// Update is called once per frame
	protected override void Update ()
    {
        if( sleeping ) return;
        base.Update();
    }

    // protected methods ///////////////////////////////////////////////////////
    protected override void WasHit(float damage)
    {
    }

    protected override void WasKilled()
    {
    }

    protected override void DoMovement()
    {
        if (curNewMoveCooldown <= 0)
        {
            curNewMoveCooldown = UnityEngine.Random.Range(newMoveCooldownMin, newMoveCooldownMax);
            curRightMovement = UnityEngine.Random.Range(minTopMovement, maxTopMovement);
            curTopMovement = UnityEngine.Random.Range(minRightMovement, maxRightMovement);
            curRotataion = UnityEngine.Random.Range(rotationMin, rotationMax);
        }
        else
        {
            if(MoveTowardsPlayer)
            {
                Vector3 heading = player.transform.position - transform.position;
                float distance = heading.magnitude;
                Vector3 direction = heading / distance;

                MoveInDirection(direction);
                LookAt(player.transform.position);

            }
            else
            {
                if (!MoveInFacingDirection)
                {
                    MoveInDirection(curTopMovement, curRightMovement);
                }
                else
                {
                    MoveInDirection(transform.forward);
                }
                RotateTo(Quaternion.AngleAxis(curRotataion, Vector3.up));
            }

            curNewMoveCooldown -= Time.deltaTime;
        }
    }

    protected override void DidTouch(GameObject touched)
    {
        if(DoesDamageOnTouch)
        {
            if(touched.tag == "Player")
            {
                Player player = touched.GetComponent<Player>();
                if(player)
                {
                    player.TakeDamage(DamageOnTouch);
                }
            }
        }
    }
}
