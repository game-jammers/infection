//
// Game Jammers 2016
//

using blacktriangles;
using UnityEngine;
using System.Collections;

namespace GameJammers.Infection
{
    public class HarveyHeart
        : DefaultEnemy
    {
        // members /////////////////////////////////////////////////////////////
        public Transform mesh;
        public GameObject deathFx;

        public float spawnRadius                            = 2.0f;
        public int spawnCount                               = 10;
        public float waveTime                               = 5.0f;

        // unity callbacks /////////////////////////////////////////////////////
        protected override void Start()
        {
            base.Start();
            StartCoroutine( SpawnEnemies() );
        }

        // enemy callbacks /////////////////////////////////////////////////////
        protected override void DoMovement()
        {
            // we don't move so don't call the base class
             if( sceneController == null ) return;
             if( sceneController.ready == false ) return;
             mesh.LookAt( player.transform, Vector3.up );
        }

        protected override void WasKilled()
        {
            base.WasKilled();
            sceneController.FinishLevel();
            Instantiate( deathFx, transform.position, Quaternion.identity );
        }

        // private methods /////////////////////////////////////////////////////
        private IEnumerator SpawnEnemies()
        {
            while( true )
            {
                if( sleeping )
                {
                    yield return new WaitForSeconds( 0.1f );
                    continue;
                }

                yield return new WaitForSeconds( waveTime / 2.0f );

                for( int i = 0; i < spawnCount; ++i )
                {
                    float radAng = (2 * Mathf.PI / spawnCount) * i;
                    float x = spawnRadius * Mathf.Sin(radAng) + transform.position.x;
                    float y = -1.0f;
                    float z = spawnRadius * Mathf.Cos(radAng) + transform.position.z;

                    Vector3 point = new Vector3( x, y, z );
                    GameObject enemyGo = Instantiate( sceneController.enemyPrefabs.Random(), point, Quaternion.identity ) as GameObject;
                    Vector3 forward = (point - transform.position).CloneWithY(-1.0f).normalized;
                    enemyGo.transform.LookAt( enemyGo.transform.position + forward, Vector3.up );
                }

                yield return new WaitForSeconds( waveTime / 2.0f );
            }
        }
    }
}
