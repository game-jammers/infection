﻿using UnityEngine;
using System.Collections;
using System;

public class EnemyShootingEasy
    : DefaultEnemy
{
    // members /////////////////////////////////////////////////////////////////
    public float shootingCooldownMax = 2.0f;
    public float shootingCooldownMin = 1.0f;
    float curShootingCooldown = 1.0f;

    public bool ShootingAtPlayer = true;
    public bool CanShoot = true;

    // unity callbacks /////////////////////////////////////////////////////////
    protected override void Awake ()
    {
        base.Awake();
        //AddWeapon<DefaultWeapon>();
    }

	protected override void Update ()
    {
        if( sleeping ) return;
        base.Update();
        DoShooting();

        SynchWeapons();
    }

    // protected methods ///////////////////////////////////////////////////////
    protected override void DoMovement()
    {
        base.DoMovement();
    }

    protected override void WasHit(float damage)
    {
        base.WasHit(damage);
    }

    protected override void WasKilled()
    {
        base.WasKilled();
    }

    protected override void DidTouch(GameObject touched)
    {
        base.DidTouch(touched);
    }

    // private methods /////////////////////////////////////////////////////////
    private void DoShooting()
    {
        if (CanShoot)
        {
            if (curShootingCooldown <= 0)
            {
                curShootingCooldown = UnityEngine.Random.Range(shootingCooldownMin, shootingCooldownMax);

                Vector3 directionVector = transform.forward;
                if (ShootingAtPlayer)
                {
                    if (player == null) return;
                    Vector3 diff = transform.position - player.transform.position;
                    if (diff.sqrMagnitude > 500.0f) return;

                    var heading = player.transform.position - transform.position;
                    var distance = heading.magnitude;
                    var direction = heading / distance;

                    direction.y = 0;

                    directionVector = direction;
                }
                foreach (Weapon weapon in Weapons)
                {
                    weapon.TryFire(directionVector);
                }
            }
            else
            {
                curShootingCooldown -= Time.deltaTime;
            }
        }
    }
}
