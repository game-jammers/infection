﻿using UnityEngine;
using System.Collections;
using System;

public class DefaultWeapon : Weapon
{

    // Use this for initialization
    protected override void Awake ()
    {
        base.Awake();
	}

	// Update is called once per frame
	void Update () {

    }

    protected override void couldNotFire()
    {
        //print("Could not fire");
    }

    protected override void doFireVisual(Vector3 direction)
    {
        //Vector3 forward = transform.TransformDirection(Vector3.forward) * 3;
        //Debug.DrawRay(transform.position, forward, Color.green, 0.1f);
        //print("fire visual");
    }
}
