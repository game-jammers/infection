﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameJammers.Infection;

public abstract class Enemy
    : WeaponCarrier
{
    // members /////////////////////////////////////////////////////////////////
    public GameSceneController sceneController = null;
    protected Player player;

    public AudioClip HitClip;

    public float Health = 1.0f;

    public bool Moving = true;

    public bool DoesDamageOnTouch = false;
    public float DamageOnTouch = 0.3f;

    public List<GameObject> Drops = new List<GameObject>();

    public bool sleeping = true;

    // private methods //////////////////////////////////////////////////////////
    private void Drop()
    {
        foreach(GameObject drop in Drops)
        {
            Pickupable pickupable = drop.GetComponent<Pickupable>();
            if(pickupable)
            {
                float randomChance = Random.Range(0f, 1f);
                if(randomChance < pickupable.Dropchance)
                {
                    GameObject dropClone = Instantiate<GameObject>(drop);
                    dropClone.transform.position = transform.position;

                    dropClone.GetComponent<Pickupable>().CalculateHighAndLowPos();

                    break;
                }
            }
        }
    }

    // public methods //////////////////////////////////////////////////////////
    public void WasHitByProjectile(float damage)
    {
        TakeDamage(damage);
    }

    public void TakeDamage(float damage)
    {
        if (HitClip)
        {
            AudioSource.PlayClipAtPoint(HitClip, transform.position);
        }

        Health -= damage;
        WasHit(damage);
        if (Health <= 0)
        {
            Drop();
            WasKilled();
            Destroy(gameObject);
        }
    }

    // unity callbacks /////////////////////////////////////////////////////////
    protected override void Start ()
    {
        base.Start();
        sceneController = GameSceneController.instance as GameSceneController;
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag != gameObject.tag)
        {
            DidTouch(other.gameObject);
        }
        else if( other.gameObject.tag == GameJammers.Infection.EnvironmentTags.WakeUpArea )
        {
            sleeping = false;
        }
    }

     void OnTriggerStay(Collider other)
     {
         if( other.gameObject.tag == GameJammers.Infection.EnvironmentTags.WakeUpArea )
         {
             sleeping = false;
         }
     }

    void OnTriggerExit(Collider other)
    {
        if( other.gameObject.tag == GameJammers.Infection.EnvironmentTags.WakeUpArea )
        {
            sleeping = true;
        }
    }

    protected override void Update ()
    {
        if( sleeping )
        {
            rb.velocity = Vector3.zero;
            rb.Sleep();
        }

        base.Update();

        if(sceneController)
        {
            if (!sceneController.ready)
            {
                return;
            }

            player = sceneController.player;
        }

        if(Moving)
        {
            DoMovement();
        }

        WeaponUpdates();
    }

    // protected methods ///////////////////////////////////////////////////////
    protected abstract void WasKilled();

    protected abstract void WasHit(float damage);

    protected abstract void DoMovement();

    protected abstract void DidTouch(GameObject touched);
}
