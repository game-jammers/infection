﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class Weapon : MonoBehaviour
{
    [HideInInspector]
    public GameObject owner;
    public Player player                                    { get; private set; }

    //degrees
    public float Spread = 0.05f;

    //Seconds
    public float ShootCooldown = 0.025f;
    public float randomWaitTimeMax = 0.2f;
    public float randomWaitTimeMin = 0.1f;

    //How many Bullets per shot?
    public int BulletsPerShot = 1;

    private float currentShootCooldown;

    [HideInInspector]
    public bool Instantiated = false;

    //prefab names
    public List<GameObject> projectiles = new List<GameObject>();

    public AudioClip FireSound;

    protected virtual void Awake()
    {
    }

	public void UpdateCooldown ()
    {
        if (currentShootCooldown > 0)
            currentShootCooldown -= Time.deltaTime;
	}

    public void TryFire(Vector3 direction)
    {
        Transform spawnPoint = owner.transform;

        player = owner.GetComponent<Player>();
        if ( player != null )
        {
            spawnPoint = player.turretSpawnPoint.transform;
        }
        
        if(currentShootCooldown <= 0)
        {
            currentShootCooldown = ShootCooldown + Random.Range(randomWaitTimeMin, randomWaitTimeMax);
            for(int i = 0; i < BulletsPerShot; i++)
            {
                doFireVisual(direction);

                GameObject projectileGo = instantiateRandomProjectile();
                projectileGo.transform.position = spawnPoint.position;

                Projectile projectile = projectileGo.GetComponent<Projectile>();
                direction = spawnPoint.forward.CloneWithY(0.0f).normalized;
                direction.x += Random.Range(-Spread, Spread);
                direction.z += Random.Range(-Spread, Spread);

                projectile.Fire(direction);
                projectile.WeaponShotWith = this;
            }
            if(FireSound)
            {
                AudioSource.PlayClipAtPoint(FireSound, transform.position);
            }
        }
        else
        {
            couldNotFire();
        }
    }

    private GameObject instantiateRandomProjectile()
    {
        int cur = Random.Range(0, projectiles.Count);
        GameObject go = (GameObject)Instantiate(projectiles[cur]);
        return go;
    }

    protected abstract void doFireVisual(Vector3 direction);

    protected abstract void couldNotFire();
}
