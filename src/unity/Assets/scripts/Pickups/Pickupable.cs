﻿using UnityEngine;
using System.Collections;

public abstract class Pickupable : MonoBehaviour
{
    public float Dropchance = 0.05f;

    public bool DoesDestroyOnEnemyTouch = false;
    public bool DoesDestroyOnBulletTouch = false;

    public bool DoesHover = true;

    public float MaxUpHover = -0.3f;
    public float MinUpHover = -0.1f;

    public float TravelTime = 3f;
    private float startTime;

    public float RotationSpeed = 20.0f;

    [HideInInspector]
    public Vector3 highPos, lowPos;
    private bool isUp = false;

    public AudioClip PickupClip;

	// Use this for initialization
	void Awake ()
    {
        CalculateHighAndLowPos();
        startTime = Time.time;
    }

    public void CalculateHighAndLowPos()
    {
        highPos = transform.position.CloneWithY(transform.position.y + MaxUpHover);
        lowPos = transform.position.CloneWithY(transform.position.y + MinUpHover);
    }
	
	// Update is called once per frame
	void Update ()
    {
        MoveUpDown();
    }

    private void MoveUpDown()
    {
        float fracComplete = (Time.time - startTime) / TravelTime;
        if (isUp)
        {
            transform.position = Vector3.Slerp(transform.position, lowPos, fracComplete);
        }
        if(!isUp)
        {
            transform.position = Vector3.Slerp(transform.position, highPos, fracComplete);
        }

        if(Vector3.Distance(transform.position, highPos) < 0.01)
        {
            isUp = true;
            startTime = Time.time;
        }
        if(Vector3.Distance(transform.position, lowPos) < 0.01)
        {
            isUp = false;
            startTime = Time.time;
        }

        transform.Rotate(Vector3.forward, RotationSpeed * Time.deltaTime);
    }

    void OnTriggerEnter(Collider other)
    {
        Enemy enemy = other.GetComponentInChildren<Enemy>();
        if(enemy && DoesDestroyOnEnemyTouch)
        {
            Destroyed(other.gameObject);
            Destroy(gameObject);
        }

        Projectile projectile = other.GetComponentInChildren<Projectile>();
        if(projectile && DoesDestroyOnBulletTouch)
        {
            Destroyed(other.gameObject);
            Destroy(gameObject);
        }
        
        Player player = other.GetComponentInChildren<Player>();
        if (player)
        {
            Destroy(gameObject);
            PickedUp(other.gameObject);

            if (PickupClip)
            {
                AudioSource.PlayClipAtPoint(PickupClip, transform.position);
            }
        }
    }

    protected abstract void PickedUp(GameObject picker);

    protected abstract void Destroyed(GameObject destroyer);
}
