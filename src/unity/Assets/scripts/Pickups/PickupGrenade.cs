﻿using UnityEngine;
using System.Collections;

public class PickupGrenade : Pickupable
{
    protected override void PickedUp(GameObject picker)
    {
        Player player = picker.GetComponentInChildren<Player>();
        if (player)
        {
            //give player grenade!
        }
    }

    protected override void Destroyed(GameObject destroyer)
    {
        //make puuff
    }
}