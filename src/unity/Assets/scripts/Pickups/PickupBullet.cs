﻿using UnityEngine;
using System.Collections;

public class PickupBullet : Pickupable
{
    // members /////////////////////////////////////////////////////////////////
    public float PowerupDuration = 2.0f;
    public float ShootCooldownDivider = 1.2f;

    Player player;

    // private methods /////////////////////////////////////////////////////////
    private IEnumerator DisablePowerupAfterDelay(Player player, float delay)
    {
        print("Start bulletpickup");
        yield return new WaitForSeconds(delay);
        print("delay finished");
        
        foreach (Weapon weapon in player.Weapons)
        {
            weapon.randomWaitTimeMax *= ShootCooldownDivider;
            weapon.randomWaitTimeMin *= ShootCooldownDivider;
        }
        print("End bulletpickup");
    }

    protected override void PickedUp(GameObject picker)
    {
        player = picker.GetComponentInChildren<Player>();
        if (player)
        {
            //give player Bullets!
            foreach (Weapon weapon in player.Weapons)
            {
                weapon.randomWaitTimeMax /= ShootCooldownDivider;
                weapon.randomWaitTimeMin /= ShootCooldownDivider;
            }
            StartCoroutine(DisablePowerupAfterDelay(player, PowerupDuration));

            print("bulletpickup");
        }
    }

    protected override void Destroyed(GameObject destroyer)
    {
        //make puuff
    }
}