﻿using UnityEngine;
using System.Collections;
using System;

public class PickupShotgun : Pickupable
{
    protected override void PickedUp(GameObject picker)
    {
        Player player = picker.GetComponentInChildren<Player>();
        if(player)
        {
            //give player shotgun!
        }
    }

    protected override void Destroyed(GameObject destroyer)
    {
        //make puuff
    }
}
