﻿using UnityEngine;
using System.Collections;

public class PickupNucleidAcid 
    : Pickupable
{
    public float HealthBonusOnPickup = 0.1f;

    protected override void Destroyed(GameObject destroyer)
    {

    }

    protected override void PickedUp(GameObject picker)
    {
        Player player = picker.GetComponentInChildren<Player>();
        if (player)
        {
            player.AddHealth(HealthBonusOnPickup);
        }
    }
}
