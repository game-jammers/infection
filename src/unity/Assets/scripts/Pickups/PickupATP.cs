﻿using UnityEngine;
using System.Collections;
using System;

public class PickupATP 
    : Pickupable
{

    public float ATPBonusOnPickup = 0.3f;

    protected override void Destroyed(GameObject destroyer)
    {

    }

    protected override void PickedUp(GameObject picker)
    {
        Player player = picker.GetComponentInChildren<Player>();
        if(player)
        {
            player.AddATP(ATPBonusOnPickup);
        }
    }

}
