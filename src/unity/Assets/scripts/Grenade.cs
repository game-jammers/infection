﻿using UnityEngine;
using System.Collections;

public class Grenade
    : MonoBehaviour
{
    public float TimeUntilExplosion = 1.5f;
    public bool ExplodeOnEnemyTouch = true;
    public bool ExplodeOnWallTouch = false;
    public float ExplosionRadius = 0.7f;
    public float ExplosionDamage = 1.0f;

    public AudioClip ExplosionClip;
    public GameObject ExplosionFx;

    public void SetParamaeterFromGrenade(Grenade grenade)
    {

    }

    void Update()
    {
        TimeUntilExplosion -= Time.deltaTime;
        if (TimeUntilExplosion <= 0)
            Explode();
    }

    public void Explode()
    {
        Collider[] objectsInRange = Physics.OverlapSphere(transform.position, ExplosionRadius);
        foreach (Collider col in objectsInRange)
        {
            Enemy enemy = col.GetComponentInChildren<Enemy>();
            if(enemy)
            {
                enemy.TakeDamage(ExplosionDamage);
            }
        }

        if(ExplosionClip)
        {
            AudioSource.PlayClipAtPoint(ExplosionClip, transform.position);
        }

        if(ExplosionFx)
        {
            Instantiate(ExplosionFx, transform.position, Quaternion.identity);
        }

        Destroy(gameObject);
    }

    void OnTriggerEnter(Collider other)
    {
        if(ExplodeOnEnemyTouch && other.gameObject.tag == "Enemy")
        {
            Explode();
        }
        if(ExplodeOnWallTouch && other.gameObject.tag == "Wall")
        {
            Explode();
        }
    }
}
