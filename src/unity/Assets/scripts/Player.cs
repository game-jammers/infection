﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player
    : WeaponCarrier
{
    // members /////////////////////////////////////////////////////////////////
    public float Health = 1.0f;
    public bool Alive = true;
    public bool Godmode = false;
    public float ATP = 1.0f;
    public float ATPRegen = 0.01f;

    private AudioSource AudioSourceMovement;
    private AudioSource AudioSourceFastMovement;
    public AudioClip MovementSound;
    public AudioClip FastMovementSound;

    public List<UseableSkill> Skills = new List<UseableSkill>();
    private List<KeyCode> SkillKeys = new List<KeyCode>() {
            KeyCode.Alpha1, KeyCode.Alpha2, KeyCode.Alpha3, KeyCode.Alpha4, KeyCode.Alpha5
        };
    public GameObject turretSpawnPoint = null;

    // public methods //////////////////////////////////////////////////////////
    public void TakeDamage(float damage)
    {
        if(!Godmode)
        {
            Health -= damage;
            if (Health <= 0)
            {
                Alive = false;
            }
        }
    }

    public void AddATP(float amount)
    {
        ATP += amount;
        if(ATP > 1.0f)
        {
            ATP = 1.0f;
        }
    }

    public void AddHealth(float amount)
    {
        Health += amount;
        if(Health > 1.0f)
        {
            Health = 1.0f;
        }
        if(amount > 0)
        {
            Alive = true;
        }
    }

    public void ShootWeapons()
    {
        if (Input.GetMouseButton(0))
        {
            foreach (Weapon weapon in Weapons)
            {
                weapon.TryFire(transform.forward);
            }
        }
    }

    private float MovementSoundFade = 2f;
    private float MovementSoundMinimum = 0.1f;

    public void PlayMovementSound()
    {
        //if(this.AudioSourceMovement)
        //{
        //    this.AudioSourceMovement.volume += MovementSoundFade * Time.deltaTime;
        //}
    }

    public void StopMovementSound()
    {
        //this.AudioSourceMovement.volume -= MovementSoundFade * Time.deltaTime;
        //if (this.AudioSourceMovement.volume < MovementSoundMinimum)
        //    this.AudioSourceMovement.volume = MovementSoundMinimum;
    }

    public Skill AddSkill <Skill>()
        where Skill:UseableSkill
    {
        Skill skill = gameObject.AddComponent<Skill>();
        skill.Equip( this );
        Skills.Add(skill);
        return skill;
    }

    public void ActivateSkill(int index)
    {
        for (int i = 0; i < Skills.Count; i++)
        {
            Skills[i].IsActive = (i == index);
        }
    }

    public void UseSkills()
    {
        if(Input.GetMouseButton(1))
        {
            foreach(UseableSkill skill in Skills)
            {
                skill.TryToActivate();
            }
        }
    }

	// unity callbacks /////////////////////////////////////////////////////////
	protected override void Start ()
    {
        base.Start();
        //AddWeapon<DefaultWeapon>();
        AddSkill<SkillGrenade>();
        AddSkill<SkillShotgun>();
        Skills[1].IsActive = true;

        //this.AudioSourceMovement = GetComponents<AudioSource>()[0];
        //this.AudioSourceMovement.volume = MovementSoundMinimum;
        //this.AudioSourceMovement.clip = MovementSound;
        //this.AudioSourceMovement.Play();
        //this.AudioSourceFastMovement = GetComponents<AudioSource>()[1];
        //this.AudioSourceFastMovement.volume = MovementSoundMinimum;

    }

    protected override void Update()
    {
        base.Update();
        if(Alive)
        {
            AddATP(ATPRegen * Time.deltaTime);

            //weapons
            WeaponUpdates();
            ShootWeapons();

            //abilities?
            CheckForSkillActivation();
            UseSkills();
        }
    }

    protected virtual void FixedUpdate()
    {
        if(Alive)
        {
            //movement
            FixedMovement();
            FixedRotation();
        }

        SynchWeapons();
    }

    // private methods /////////////////////////////////////////////////////////
    private void CheckForSkillActivation()
    {
        for (int i = 0; i < SkillKeys.Count; i++)
        {
            if (Input.GetKey(SkillKeys[i]))
            {
                ActivateSkill(i);
            }
        }
    }

    private void FixedMovement()
    {
        //Movement
        float vertical = Input.GetAxis("Vertical");
        float horizontal = Input.GetAxis("Horizontal");

        //if(vertical != 0 || horizontal != 0)
        //{
        //    PlayMovementSound();
        //}
        //else
        //{
        //    StopMovementSound();
        //}

        rb.velocity += new Vector3(horizontal, 0f, vertical) * MovementSpeed;
    }

    private void FixedRotation()
    {
        //http://wiki.unity3d.com/index.php?title=LookAtMouse
        //Rotation
        // Generate a plane that intersects the transform's position with an upwards normal.
        Plane playerPlane = new Plane(Vector3.up, transform.position);

        // Generate a ray from the cursor position
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        // Determine the point where the cursor ray intersects the plane.
        // This will be the point that the object must look towards to be looking at the mouse.
        // Raycasting to a Plane object only gives us a distance, so we'll have to take the distance,
        //   then find the point along that ray that meets that distance.  This will be the point
        //   to look at.
        float hitdist = 0.0f;
        // If the ray is parallel to the plane, Raycast will return false.
        if (playerPlane.Raycast(ray, out hitdist))
        {
            // Get the point along the ray that hits the calculated distance.
            Vector3 targetPoint = ray.GetPoint(hitdist);

            LookAt(targetPoint);
        }
    }
}
