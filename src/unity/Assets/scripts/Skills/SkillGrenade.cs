﻿using UnityEngine;
using System.Collections;
using System;

public class SkillGrenade
    : UseableSkill
{
    public override string Icon { get { return "icons/grenade"; } }
    public float MinThrowingDistance = 90f;
    public float MaxThrowingDistance = 110f;

    public bool ExplodeOnEnemyTouch = true;
    public bool ExplodeOnWallTouch = false;

    public GameObject GrenadeVisual;

    public Grenade grenade;

    void Start()
    {
        ATPCost = 0.3f;
    }

    protected override void Activate()
    {
        GrenadeVisual = (GameObject)Instantiate(Resources.Load("grenadeThrowable"));

        GrenadeVisual.transform.position = player.transform.position.CloneWithY(1.0f);
        Rigidbody rigidBody = GrenadeVisual.GetComponent<Rigidbody>();
        if(rigidBody)
        {
            Plane playerPlane = new Plane(Vector3.up, transform.position);
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            float hitdist = 0.0f;
            if (playerPlane.Raycast(ray, out hitdist))
            {
                Vector3 targetPoint = ray.GetPoint(hitdist);
                targetPoint -= GrenadeVisual.transform.position;

                rigidBody.AddForce(targetPoint * UnityEngine.Random.Range(MinThrowingDistance,
                                                                          MaxThrowingDistance));

                Grenade grenadeParameter = GrenadeVisual.GetComponent<Grenade>();
                if(grenade)
                {
                    grenadeParameter.SetParamaeterFromGrenade(grenade);
                }
            }
        }
    }
}
