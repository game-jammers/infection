﻿using UnityEngine;
using System.Collections;

public abstract class UseableSkill
    : MonoBehaviour
{
    [HideInInspector]
    public bool IsActive = false;
    protected Player player;

    public string Name = "Unknown Skill";
    public abstract string Icon { get; }
    public float ATPCost = 0.1f;
    public float Cooldown = 1.0f;
    private float CurrentCooldown = 0.0f;
    public float progress { get { return 1.0f - CurrentCooldown / Cooldown; } }

    public void Equip( Player _player )
    {
        player = _player;
    }

    public void TryToActivate()
    {
        if(IsActive)
        {
            if(CurrentCooldown <= 0 && player.ATP > ATPCost )
            {
                player.ATP -= ATPCost;
                Activate();
                CurrentCooldown = Cooldown;
            }
        }
    }

    protected abstract void Activate();

    protected virtual void Update()
    {
        if(CurrentCooldown > 0)
        {
            CurrentCooldown -= Time.deltaTime;
        }
    }
}
