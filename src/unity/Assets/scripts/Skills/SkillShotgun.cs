﻿using UnityEngine;
using System.Collections;
using System;

public class SkillShotgun 
    : UseableSkill
{
    public override string Icon { get { return "icons/shotgun"; } }

    private Weapon weapon;
    void Start()
    {
        ATPCost = 0.15f;
    }

    protected override void Activate()
    {
        if(!weapon)
        {
            GameObject weaponObject = Instantiate(Resources.Load("WeaponPlayerShotgun")) as GameObject;
            weapon = weaponObject.GetComponent<Weapon>();
            weapon.owner = player.gameObject;
        }

        weapon.TryFire(player.turretSpawnPoint.transform.forward);
    }

    protected override void Update()
    {
        base.Update();

        if(weapon)
            weapon.UpdateCooldown();
    }
}
