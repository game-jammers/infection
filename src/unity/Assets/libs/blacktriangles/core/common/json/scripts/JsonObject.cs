//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
// Based on MiniJSON by Calvin Rien [https://gist.github.com/darktable/1411710]
// which was based on Percurios JSON Parser by Patrick van Bergen [http://techblog.procurios.nl/k/618/news/view/14605/14863/How-do-I-write-my-own-parser-for-JSON.html]
//
// THIS FILE IS LICENSED UNDER MIT LICENSE
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
// SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//=============================================================================

using System.Reflection;
using System.Collections;
using System.Collections.Generic;

namespace blacktriangles
{
    public class JsonObject
        : IJsonSerializable
    {
        // members ////////////////////////////////////////////////////////////
        public Dictionary< string, object > fields              = null;

        // operators //////////////////////////////////////////////////////////
        public object this[ string name ]                       { get { return GetField( name ); } set { SetField( name, value ); } }

        public IEnumerable keys                                 { get { return fields.Keys; } }
        public IEnumerable values                               { get { return fields.Values; } }

        // constructor / destructor ///////////////////////////////////////////
        public JsonObject()
        {
            fields = new Dictionary< string, object >();
        }

        public JsonObject( Dictionary< string, object > _fields )
        {
            fields = _fields;
        }

        public static JsonObject FromString( string json )
        {
            return JsonParser.Parse( json );
        }

        public static JsonObject Clone( JsonObject json )
        {
            return FromString( json.ToString() );
        }

        // helper static methods //////////////////////////////////////////////
        public static JsonObject NormalizeObject( JsonObject obj )
        {
            return FromString( obj.ToString() );
        }

        public static JsonObject NormalizeObject( IJsonSerializable obj )
        {
            return NormalizeObject( obj.ToJson() );
        }

        public static JsonObject SafeSerialize( IJsonSerializable obj )
        {
            return obj == null ? null : obj.ToJson();
        }

        public static bool SafeDeserialize( IJsonSerializable obj, JsonObject json )
        {
            bool result = ( obj != null && json != null );
            if( result )
                obj.FromJson( json );

            return result;
        }

        public static bool SafeDeserialize( IJsonSerializable obj, JsonObject json, string fieldName )
        {
            bool result = false;
            if( obj != null && json != null )
            {
                JsonObject field = json.GetField<JsonObject>( fieldName );
                if( field != null )
                {
                    obj.FromJson( field );
                    result = true;
                }
            }

            return result;
        }

        // public methods /////////////////////////////////////////////////////
        public JsonObject Clone()
        {
            return JsonObject.Clone( this );
        }

        public bool ContainsKey( string key )
        {
            return fields.ContainsKey( key );
        }

        public object GetField( string name )
        {
            object result = null;
            fields.TryGetValue( name, out result );
            return result;
        }

        public T GetField<T>( string name )
        {
            T result = default(T);

            object field = GetField( name );
            if( field != null )
            {
                result = (T)Convert( field, typeof( T ) );
            }

            return result;
        }

        public void SetField( string name, object val )
        {
            fields[ name ] = val;
        }

        public List<T> SafeDeserializeList<T>( string fieldname )
            where T: IJsonSerializable
        {
            List<T> result = new List<T>();
            T[] array = GetField<T[]>( fieldname );
            if( array != null )
                result.AddRange( array );
            return result;
        }

        public Dictionary< KeyType, ValueType > SafeDeserializeDictionary<KeyType,ValueType>( string fieldname )
        {
          Dictionary< KeyType, ValueType > result = new Dictionary< KeyType, ValueType >();
          JsonObject sub = GetField<JsonObject>( fieldname );
          foreach( var pair in sub.fields )
          {
            KeyType key = (KeyType)Convert( pair.Key, typeof( KeyType ) );
            result[ key ] = (ValueType)Convert( pair.Value, typeof( ValueType ) );
          }

          return result;
        }

        public override string ToString()
        {
            return JsonSerializer.Serialize( this );
        }

        public JsonObject ToJson()
        {
            return this;
        }

        public void FromJson( JsonObject other )
        {
            JsonObject clone = JsonObject.Clone( other );
            fields = clone.fields;
        }

        // private methods ////////////////////////////////////////////////////
        private object Convert( object field, System.Type resultType )
        {
            object result = null;

            System.Type fieldType = field.GetType();

            // the value in the dictionary can be cast and passed back directly
            if( resultType.IsAssignableFrom( fieldType ) )
            {
                result = field;
            }
            else if( fieldType == typeof( System.String ) && resultType.IsEnum )
            {
                result = System.Enum.Parse( resultType, field.ToString() );
            }
            else if( fieldType.IsValueType )
            {
                try
                {
                    result = System.Convert.ChangeType( field, resultType );
                }
                catch( System.InvalidCastException )
                {
                    //###hsmith $TODO Console.WriteLine?
                    //UnityEngine.Debug.LogError( "Unable to cast value type to " + resultType.ToString() );
                }
            }
            else if( resultType.IsArray )
            {
                System.Type arrayItemType = resultType.GetElementType();

                System.Array fields = (System.Array)field;
                System.Array array = (System.Array)System.Activator.CreateInstance( resultType, new object[] { fields.Length } );

                for( int i = 0; i < fields.Length; ++i )
                {
                    array.SetValue( Convert( fields.GetValue(i), arrayItemType ), i );
                }

                result = array;
            }
            else if( resultType == typeof( System.DateTime ) )
            {
                result = System.DateTime.Parse( (string)field );
            }
            else if( fieldType == typeof( JsonObject ) )
            {
                if( resultType.GetInterfaces().Contains( typeof( IJsonSerializable ) ) )
                {
                    if( result == null )
                    {
                        result = System.Activator.CreateInstance( resultType );
                    }

                    ((IJsonSerializable)result).FromJson( (JsonObject)field );
                }
                else
                {
                    // check to see if we have an extension method to read from json
                    MethodInfo method = resultType.GetExtensionMethod( "FromJson" );
                    if( method != null )
                    {
                        ParameterInfo[] info = method.GetParameters();
                        if( info != null )
                        {
                            if( resultType.IsValueType )
                            {
                                if( info.Length == 2
                                    && info[0].ParameterType == resultType
                                    && info[1].ParameterType == typeof( JsonObject )
                                    && method.ReturnType == resultType
                                )
                                {
                                    result = method.Invoke( null, new object[]{ null, field } );
                                }
                            }
                            else
                            {
                                if( info.Length == 2
                                    && info[0].ParameterType == resultType
                                    && info[1].ParameterType == typeof( JsonObject )
                                )
                                {
                                    result = System.Activator.CreateInstance( resultType );
                                    method.Invoke( null, new object[]{ result, field } );
                                }
                            }
                        }
                    }
                }
            }

            return result;
        }
    }
}
