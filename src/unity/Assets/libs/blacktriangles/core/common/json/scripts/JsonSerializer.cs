//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
// Based on MiniJSON by Calvin Rien [https://gist.github.com/darktable/1411710]
// which was based on Percurios JSON Parser by Patrick van Bergen [http://techblog.procurios.nl/k/618/news/view/14605/14863/How-do-I-write-my-own-parser-for-JSON.html]
//
// THIS FILE IS LICENSED UNDER MIT LICENSE
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
// SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//=============================================================================

using System.Text;
using System.Collections;
using System.Reflection;

namespace blacktriangles
{
    public class JsonSerializer
    {
        // members ////////////////////////////////////////////////////////////
        private StringBuilder builder                           = null;

        // constructor / initializer //////////////////////////////////////////
        public static string Serialize( JsonObject obj )
        {
            JsonSerializer serializer = new JsonSerializer();
            serializer.SerializeValue( obj );
            return serializer.builder.ToString();
        }

        private JsonSerializer()
        {
            builder = new StringBuilder();
        }

        // private methods ////////////////////////////////////////////////////
        private void SerializeValue( object value )
        {
            if( value == null ) {
                builder.Append("null");
            }
            else if( value as string != null ) {
                SerializeString( value as string );
            }
            else if( value is bool ) {
                builder.Append(value.ToString().ToLower());
            }
            else if( value as JsonObject != null ) {
                SerializeObject(value as JsonObject );
            }
            else if( value as IDictionary != null ) {
                SerializeDictionary(value as IDictionary );
            }
            else if( value as IList != null ) {
                SerializeArray( value as IList );
            }
            else if( value is char) {
                SerializeString( value.ToString() );
            }
            else if( value is System.DateTime ) {
                System.DateTime val = (System.DateTime)value;
                SerializeString( val.ToString( "s" ) + "Z" );
            }
            else if( value is IJsonSerializable ) {
                JsonObject obj = (value as IJsonSerializable).ToJson();
                SerializeObject( obj );
            }
            else if( value.GetType().IsEnum ) {
                SerializeString( value.ToString() );
            }
            else {
                MethodInfo info = value.GetType().GetExtensionMethod( "ToJson" );
                if( info != null && info.ReturnType == typeof( JsonObject ) )
                {
                    JsonObject obj = (JsonObject)info.Invoke( value, new object[] { value } );
                    SerializeObject( obj );
                }
                else
                {
                    SerializeOther( value );
                }
            }
        }

        private void SerializeObject( JsonObject obj )
        {
            SerializeDictionary(obj.fields);
        }

        private void SerializeDictionary( IDictionary obj )
        {
            bool first = true;

            builder.Append('{');

            foreach( object e in obj.Keys )
            {
                if( !first )
                {
                    builder.Append(',');
                }

                SerializeString( e.ToString() );
                builder.Append(':');

                SerializeValue( obj[e] );

                first = false;
            }

            builder.Append('}');
        }

        private void SerializeArray( IList anArray )
        {
            builder.Append('[');

            bool first = true;

            foreach( object obj in anArray )
            {
                if( !first )
                {
                    builder.Append(',');
                }

                SerializeValue(obj);

                first = false;
            }

            builder.Append(']');
        }

        private void SerializeString( string str )
        {
            builder.Append('\"');

            char[] charArray = str.ToCharArray();
            foreach( var c in charArray )
            {
                switch( c )
                {
                case '"':
                    builder.Append("\\\"");
                    break;
                case '\\':
                    builder.Append("\\\\");
                    break;
                case '\b':
                    builder.Append("\\b");
                    break;
                case '\f':
                    builder.Append("\\f");
                    break;
                case '\n':
                    builder.Append("\\n");
                    break;
                case '\r':
                    builder.Append("\\r");
                    break;
                case '\t':
                    builder.Append("\\t");
                    break;
                default:
                    int codepoint = System.Convert.ToInt32(c);
                    if( (codepoint >= 32) && (codepoint <= 126) )
                    {
                        builder.Append(c);
                    }
                    else
                    {
                        builder.Append("\\u" + System.Convert.ToString(codepoint, 16).PadLeft(4, '0'));
                    }
                    break;
                }
            }

            builder.Append('\"');
        }

        void SerializeOther(object value)
        {
            if( value is float
                || value is int
                || value is uint
                || value is long
                || value is double
                || value is sbyte
                || value is byte
                || value is short
                || value is ushort
                || value is ulong
                || value is decimal)
            {
                builder.Append(value.ToString());
            }
            else
            {
                SerializeString(value.ToString());
            }
        }
    }
}
