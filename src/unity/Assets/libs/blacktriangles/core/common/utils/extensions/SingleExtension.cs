//=============================================================================
//
// (C) BLACKTRIANGLES 2016
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

namespace blacktriangles
{
  public static class SingleExtension
  {
    public static byte ToByte( this float self )
    {
      return (byte)btMath.Round( 255 * self );
    }
  }
}
