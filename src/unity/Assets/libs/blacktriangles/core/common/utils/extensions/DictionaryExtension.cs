//=============================================================================
//
// (C) BLACKTRIANGLES 2016
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System.Collections.Generic;

namespace blacktriangles
{
  public static class DictionaryExtension
  {
    public static U Find<T,U>( this Dictionary<T,U> self, T key )
      where U: class
    {
      U result = null;
      if( self.TryGetValue( key, out result ) == false )
        return null;

      return result;
    }

    public static void Merge<T,U>( this Dictionary<T,U> self, Dictionary<T,U> other, bool overwrite = false )
    {
      foreach( KeyValuePair<T,U> pair in other )
      {
        if( self.ContainsKey( pair.Key ) == false || overwrite )
        {
          self[pair.Key] = other[pair.Key];
        }
      }
    }
  }
}
