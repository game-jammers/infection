//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//=============================================================================

namespace blacktriangles
{
	public static class Performance
	{
		public static void Profile( string name, System.Action command )
		{
			#if BT_DEBUG || UNITY_EDITOR
			System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
			timer.Start();
			#endif

			command();

			#if BT_DEBUG || UNITY_EDITOR
			timer.Stop();
			DebugUtility.Log( name + " took " + timer.Elapsed.ToString() );
			#endif
		}
	}
}
