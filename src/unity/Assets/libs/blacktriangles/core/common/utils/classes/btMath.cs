//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

namespace blacktriangles
{
	public static class btMath
	{
		public static float Clamp( float value, float min, float max )
		{
			return Max( Min( max, value ), min );
		}

		public static float Wrap( float value, float min, float max )
		{
			float range = max - min;
			value = value % range;
			return min + value;
		}

		public static int Wrap( int value, int min, int max )
		{
			int range = max - min;
			value = value % range;
			return min + value;
		}

		public static bool Approximately( float lhs, float rhs )
		{
      		return (Abs(lhs - rhs) <= System.Single.Epsilon);
		}

		public static float Floor( float val )
		{
			return (float)System.Math.Floor( val );
		}

		public static float Round( float val )
		{
			return (float)System.Math.Round( val );
		}

		public static float Ceil( float val )
		{
			return (float)System.Math.Ceiling( val );
		}

		public static float Abs( float val )
		{
			return (float)System.Math.Abs( val );
		}

		public static float Sqrt( float val )
		{
			return (float)System.Math.Sqrt( val );
		}

		public static float Min( float lhs, float rhs )
		{
			return (float)System.Math.Min( lhs, rhs );
		}

		public static float Max( float lhs, float rhs )
		{
			return (float)System.Math.Max( lhs, rhs );
		}

		public static float Sin( float lhs )
		{
			return (float)System.Math.Sin( lhs );
		}

		public static float Cos( float lhs )
		{
			return (float)System.Math.Cos( lhs );
		}

		public static float Acos( float lhs )
		{
			return (float)System.Math.Acos( lhs );
		}

		public static float Tan( float lhs )
		{
			return (float)System.Math.Tan( lhs );
		}

    	public static float Log( float n )
    	{
      	return (float)System.Math.Log( n );
    	}

    	public static float PI                                  { get { return (float)System.Math.PI; } }
			public static float Deg2Rad								              { get { return (float)System.Math.PI / 180f; } }
		}
}
