//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System.Collections.Generic;

public static class HashSetExtension
{
  public static T[] ToArray<T>( this HashSet<T> self )
  {
    T[] result = new T[ self.Count ];
    int index = 0;
    foreach( T item in self )
    {
        result[ index++ ] = item;
    }

    return result;
  }
}
