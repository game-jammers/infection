//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System.Collections.Generic;

namespace blacktriangles
{
  public class Flags< FlagType >
    where FlagType: System.IConvertible
  {
    // members ////////////////////////////////////////////////////////////////
    private int value                                           = 0;

    // constructor / destructor ///////////////////////////////////////////////
    public Flags()
    {
      CheckType();
      SetValue( 0 );
    }

    public Flags( FlagType flag )
    {
      CheckType();
      SetValue( 0 );
      SetFlag( flag );
    }

    public Flags( int initValue )
    {
      CheckType();
      SetValue( initValue );
    }

    public Flags( FlagType[] flag )
    {
      CheckType();
      SetFlags( flag );
    }

    // public methods /////////////////////////////////////////////////////////
    public void SetValue( int newValue )
    {
      value = newValue;
    }

    public void SetFlag( FlagType flag )
    {
      value |= ToInt32( flag );
    }

    public void SetFlags( FlagType[] flags )
    {
      foreach( FlagType flag in flags )
        SetFlag( flag );
    }

    public void UnsetFlag( FlagType flag )
    {
      value &= ~(ToInt32( flag ));
    }

    public void UnsetFlags( FlagType[] flags )
    {
      foreach( FlagType flag in flags )
        UnsetFlag( flag );
    }

    public bool isFlagSet( FlagType flag )
    {
      return ( (ToInt32(flag) & value) != 0 );
    }

    public FlagType[] GetFlags()
    {
      List<FlagType> result = new List<FlagType>();
      System.Array possibleFlags = System.Enum.GetValues( typeof( FlagType ) );
      foreach( FlagType flag in possibleFlags )
      {
        if( isFlagSet( flag ) )
        {
          result.Add( flag );
        }
      }

      return result.ToArray();
    }

    // private methods ////////////////////////////////////////////////////////
    private int ToInt32( FlagType flag )
    {
      return 1 << System.Convert.ToInt32( flag );
    }

    private void CheckType()
    {
      if( typeof( FlagType ).IsEnum == false )
      {
        throw new System.ArgumentException( "FlagType must be an ENUM type" );
      }
    }
  }
}
