//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System.Collections.Generic;

public static class StringExtension
{
	public static bool Contains( this string self, char c )
	{
		return self.IndexOf( c ) != -1;
	}

  public static string[] SplitByLine( this string self )
  {
    return self.Split( '\n' );
  }
}
