//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System.Collections.Generic;
using System.Reflection;

namespace blacktriangles
{
	public static class AssemblyUtility
	{
		// public methods /////////////////////////////////////////////////////
		public static Assembly GetEditorAssembly()
		{
			Assembly[] assemblies = System.AppDomain.CurrentDomain.GetAssemblies();
			foreach( Assembly asm in assemblies )
			{
				if( asm.GetName().Name == "Assembly-CSharp-Editor" )
				{
					return asm;
				}
			}

			return null;
		}

		public static Assembly GetGameAssembly()
		{
			Assembly[] assemblies = System.AppDomain.CurrentDomain.GetAssemblies();
			foreach( Assembly asm in assemblies )
			{
				if( asm.GetName().Name == "Assembly-CSharp" )
				{
					return asm;
				}
			}

			return null;
		}

		public static string[] GetAllAssemblyNames()
		{
			List<string> result = new List<string>();
			Assembly[] assemblies = System.AppDomain.CurrentDomain.GetAssemblies();
			foreach( Assembly asm in assemblies )
 			{
				result.Add( asm.GetName().Name );
			}
			return result.ToArray();
		}

		public static System.Type[] CollectTypesWithAttribute<T>()
		{
			return CollectTypesWithAttribute<T>( Assembly.GetExecutingAssembly() );
		}

		public static System.Type[] CollectDescendantsOf<ClassType>()
			where ClassType: class
		{
			return CollectDescendantsOf<ClassType>( Assembly.GetExecutingAssembly() );
		}

		public static System.Type[] CollectEditorTypesWithAttribute<T>()
		{
			return CollectTypesWithAttribute<T>( GetEditorAssembly() );
		}

		public static System.Type[] CollectTypesWithAttribute<T>( Assembly asm  )
		{
			return CollectTypes( asm, (type)=>{
				return System.Attribute.IsDefined( type, typeof( T ) );
			});
		}

		public static System.Type[] CollectDescendantsOf<ClassType>( Assembly asm )
		{
			return CollectTypes( asm, (type)=>{
				return type.IsSubclassOf( typeof( ClassType ) );
			});
		}

		public static System.Type[] CollectWithInterface<InterfaceType>( Assembly asm, bool isInterface = false, bool isAbstract = false )
		{
			System.Type interfaceType = typeof(InterfaceType);
			return CollectTypes( asm, (type)=>{
				return ( interfaceType.IsAssignableFrom( type ) && type.IsInterface == isInterface && type.IsAbstract == isAbstract );
			});
		}

		public static System.Type[] CollectTypes( Assembly asm, System.Predicate<System.Type> predicate )
		{
			List<System.Type> result = new List<System.Type>();
			foreach( System.Type type in asm.GetTypes() )
			{
				if( predicate( type ) )
				{
					result.Add( type );
				}
			}

			return result.ToArray();
		}

		public static T GetAttribute<T>( System.Type type)
			where T: System.Attribute
		{
			T result = null;
			System.Reflection.MemberInfo info = type;
			System.Object[] attributes = info.GetCustomAttributes( typeof( T ), true );
			if( attributes != null && attributes.Length > 0 )
			{
				 result = (T)attributes[0];
			}

			return result;
		}
	}
}
