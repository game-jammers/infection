//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

namespace blacktriangles
{
  [System.Serializable]
  public class GaussianGenerator
  {
    // members ////////////////////////////////////////////////////////////////
    public float mean                             = 0.0f;
    public float variance                         = 1.0f;

    // constructor / destructor ///////////////////////////////////////////////
    public GaussianGenerator( float mean_, float variance_ )
    {
      mean = mean_;
      variance = variance_;
    }

    // public methods /////////////////////////////////////////////////////////
    public float Generate()
    {
      return btRandom.Gaussian( mean, variance );
    }
  }
}
