//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System.Collections.Generic;

namespace blacktriangles
{
	public static class Convert
	{
		public static List<OutputType> All<InputType,OutputType>( IEnumerable<InputType> items, System.Func<InputType,OutputType> convertFunc )
		{
			List<OutputType> result = new List<OutputType>();
			foreach( InputType input in items )
			{
				result.Add( convertFunc( input ) );
			}
			return result;
		}
	}
}
