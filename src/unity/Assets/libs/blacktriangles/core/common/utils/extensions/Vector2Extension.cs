//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;

public static class Vector2Extension
{
    public static Vector2 Round( this Vector2 self )
    {
        return new Vector2( btMath.Round( self.x ), btMath.Round( self.y ) );
    }

    public static Vector2 Ceil( this Vector2 self )
    {
        return new Vector2( btMath.Ceil( self.x ), btMath.Ceil( self.y ) );
    }

    public static Vector2 Floor( this Vector2 self )
    {
        return new Vector2( btMath.Floor( self.x ), btMath.Floor( self.y ) );
    }

    public static bool IsNearZero( this Vector2 self )
    {
        return ( self.x.IsNearZero() && self.y.IsNearZero() );
    }

    public static Vector3 ToVector3( this Vector2 self )
    {
        return new Vector3( self.x, self.y, 0f );
    }

    public static Vector3 ToVector3XZ( this Vector2 self )
    {
        return new Vector3( self.x, 0f, self.y );
    }

    public static Vector2 RandomRange( Vector2 min, Vector2 max )
    {
        return new Vector2( btRandom.Range( min.x, max.x ), btRandom.Range( min.y, max.y ) );
    }
}
