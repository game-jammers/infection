//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;

public static class Vector3Extension
{
    public static Vector3 CloneWithX( this Vector3 self, float x )
    {
        return new Vector3( x, self.y, self.z );
    }

    public static Vector3 CloneWithY( this Vector3 self, float y )
    {
        return new Vector3( self.x, y, self.z );
    }

    public static Vector3 CloneWithZ( this Vector3 self, float z )
    {
        return new Vector3( self.x, self.y, z );
    }

    public static Vector3 Min( Vector3 self, Vector3 other )
    {
        return new Vector3( btMath.Min( self.x, other.x ), btMath.Min( self.y, other.y ), btMath.Min( self.z, other.z ) );
    }

    public static Vector3 Max( Vector3 self, Vector3 other )
    {
        return new Vector3( btMath.Max( self.x, other.x ), btMath.Max( self.y, other.y ), btMath.Max( self.z, other.z ) );
    }

    public static bool IsNearZero( this Vector3 self )
    {
        return self == Vector3.zero;
    }

    public static Vector3 ToVector3XZ( this Vector3 self )
    {
        return self.ToVector3XZ( 0f );
    }

    public static Vector3 ToVector3XZ( this Vector3 self, float y )
    {
        return new Vector3( self.x, y, self.z );
    }

    public static Vector2 ToVector2XY( this Vector3 self )
    {
        return new Vector2( self.x, self.y );
    }

    public static Vector2 ToVector2XZ( this Vector3 self )
    {
      return new Vector2( self.x, self.z );
    }

    public static float DistanceSqr( this Vector3 self, Vector3 rhs )
    {
        return ( rhs - self ).sqrMagnitude;
    }

    public static bool IsInRangeSqr( this Vector3 self, Vector3 rhs, float range )
    {
        return self.DistanceSqr( rhs ) < ( range * range );
    }

    public static Vector3 RandomRange( Vector3 min, Vector3 max )
    {
        return new Vector3( btRandom.Range( min.x, max.x ), btRandom.Range( min.y, max.y ), btRandom.Range( min.z, max.z ) );
    }

    public static Vector3 Random()
    {
        return RandomRange( Vector3.zero, Vector3.one );
    }

	public static Vector3 RandomRange( Vector3 min, Vector3 max, System.Random rng )
	{
		return new Vector3( rng.NextFloat( min.x, max.x ), rng.NextFloat( min.y, max.y ), rng.NextFloat( min.z, max.z ) );
	}

	public static Vector3 Random( System.Random rng )
	{
		return RandomRange( Vector3.zero, Vector3.one, rng );
	}

    public static Vector3 RandomXY( System.Random rng )
	{
		return RandomRange( Vector3.zero, Vector3.one, rng );
	}

    public static Vector3 RandomUnitXZ()
    {
        return new Vector3( btRandom.Range( 0f, 1f ), 0f, btRandom.Range( 0f, 1f ) );
    }

    public static Vector3 RandomUnitXY()
    {
        return new Vector3( btRandom.Range( 0f, 1f ), btRandom.Range( 0f, 1f ), 0f );
    }
}
