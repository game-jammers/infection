//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System;
using System.Reflection;
using System.Collections.Generic;

namespace blacktriangles
{
	public static class CloneVat
	{
		// deep cloning
		public static T DeepClone<T>( T item )
		{
			System.Type type = typeof( T );

			if( type.IsValueType )
			{
				return item;
			}
			else if( item == null )
			{
				return default(T);
			}
			else if( typeof(IDeepCloneable<T>).IsAssignableFrom( type ) )
			{
				IDeepCloneable<T> cloneable = item as IDeepCloneable<T>;
				return cloneable.DeepClone();
			}
			else if( type == typeof(System.String) )
			{
				return (T)(object)System.String.Copy( (string)(object)item );
			}
			else
			{
				throw new System.NotImplementedException( "Todo: Implement deep clone via reflection or dynamic method" );
			}
		}

		public static T[] DeepClone<T>( T[] items )
		{
			if( items == null ) return null;

			T[] result = new T[ items.Length ];
			for( int i = 0; i < items.Length; ++i )
			{
				result[i] = DeepClone<T>( items[i] );
			}

			return result;
		}

		public static List<T> DeepClone<T>( List<T> list )
		{
			if( list == null ) return null;
			if( list.Count == 0 ) return new List<T>();

			List<T> result = new List<T>();
			T[] items = DeepClone( list.ToArray() );
			result.AddRange( items );
			return result;
		}

		// shallow cloning /////////////////////////////////////
		public static T[] ShallowClone<T>( T[] orig )
		{
			T[] result = new T[ orig.Length ];
			System.Array.Copy( orig, result, orig.Length );
			return result;
		}
	}
}
