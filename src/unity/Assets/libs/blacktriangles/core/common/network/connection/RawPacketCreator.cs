//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System;
using System.IO;

namespace blacktriangles.Network
{
	public class RawPacketCreator
	{
		// members ////////////////////////////////////////////////////////////
		public Packet.Header header								= default(Packet.Header);
		public NetworkBuffer headerBuffer						= new NetworkBuffer( Packet.kHeaderSize );
		public NetworkBuffer payloadBuffer						= null;

		// constructor / initializer //////////////////////////////////////////
		public RawPacketCreator()
		{
			Reset();
		}

		// public methods /////////////////////////////////////////////////////
		public RawPacket Read( BinaryReader reader )
		{
			if( headerBuffer.isFull == false )
			{
				headerBuffer.PutBytes( reader );
			}
			else if( payloadBuffer == null )
			{
				header = Packet.Header.FromNetworkBuffer( headerBuffer );
				payloadBuffer = new NetworkBuffer( header.payloadSize );
			}
			else
			{
				payloadBuffer.PutBytes( reader );
				if( payloadBuffer.isFull )
				{
					RawPacket result = new RawPacket( header.id, payloadBuffer );
					Reset();
					return result;
				}
			}

			return null;
		}

		public void Reset()
		{
			headerBuffer.Clear();
			payloadBuffer = null;
		}
	}
}
