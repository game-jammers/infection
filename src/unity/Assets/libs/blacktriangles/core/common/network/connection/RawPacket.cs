
//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System;
using System.IO;
using System.Collections.Generic;

namespace blacktriangles.Network
{
	public class RawPacket
	{
		// members ////////////////////////////////////////////////////////////
		public Connection source								= null;
		public Packet.Header header								{ get; private set; }
		public NetworkBuffer payload							{ get; private set; }

		// constructor / initializer //////////////////////////////////////////
		public RawPacket( int packetId, NetworkBuffer _payload )
		{
			header = new Packet.Header( packetId, _payload.bufferSize );
			payload = _payload;
		}

		public RawPacket( BinaryReader reader )
		{
			header = default(Packet.Header);
			payload = null;
			ReadFromStream( reader );
		}

		// public methods /////////////////////////////////////////////////////
		public void WriteToStream( BinaryWriter writer )
		{
			// validate that the header payload size matches the actual payload size;
			if( header.payloadSize != payload.bufferSize )
			{
				//###hsmith $TODO Console.WriteLine?
				//UnityEngine.Debug.LogWarning( "Packet header payload size does not match actual paylaod size!" );
			}

			NetworkBuffer headerBuffer = Packet.Header.ToNetworkBuffer( header );
			writer.Write( headerBuffer.bytes );
			writer.Write( payload.bytes );
		}

		public void ReadFromStream( BinaryReader reader )
		{
			Packet.Header header = ReadHeader( reader );
			payload = new NetworkBuffer( header.payloadSize );

			while( !payload.isFull )
			{
				payload.PutBytes( reader );
			}
		}

		// private methods ////////////////////////////////////////////////////
		private Packet.Header ReadHeader( BinaryReader reader )
		{
			NetworkBuffer headerBuffer = new NetworkBuffer( Packet.kHeaderSize );
			while( headerBuffer.isFull == false )
			{
				headerBuffer.PutBytes( reader );
			}

			return Packet.Header.FromNetworkBuffer( headerBuffer );
		}
	}
}
