
//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System;
using System.IO;
using System.Collections.Generic;

namespace blacktriangles.Network
{
	public sealed class NetworkBuffer
	{
		// members ////////////////////////////////////////////////////////////
		public byte[] bytes										{ get; private set; }

		//// reading bytes /////////////////////////////////////
		public int filledBytes									{ get { return _filledBytes; } set { SetFilledBytes( value ); } }
		public int bufferSize									{ get { return bytes.Length; } }
		public int remainingBytes								{ get { return bufferSize - filledBytes; } }
		public bool isFull										{ get { return remainingBytes == 0; } }

		private int _filledBytes								= 0;

		// constructor / destructor ///////////////////////////////////////////
		public NetworkBuffer( int size )
		{
			bytes = new byte[ size ];
		}

		public NetworkBuffer( byte[] _bytes )
		{
			bytes = _bytes;
		}

		// public members /////////////////////////////////////////////////////
		public int PutBytes( BinaryReader reader )
		{
			int bytesRead = reader.Read( bytes, filledBytes, remainingBytes );
			filledBytes += bytesRead;
			return bytesRead;
		}

		public int PutBytes( byte[] newBytes )
		{
			System.Array.Copy( newBytes, 0, bytes, filledBytes, newBytes.Length );
			filledBytes += newBytes.Length;

			return bytes.Length;
		}

		public void Clear()
		{
			filledBytes = 0;
		}

		public void DebugDump()
		{
			//###hsmith $TODO Console.WriteLine?
			//UnityEngine.Debug.Log( System.String.Format( "NetworkBuffer: Filled[{0}:{3}] | Size[{1}] | Remaining[{2}]", filledBytes, bufferSize, remainingBytes, isFull ) );
		}

		// private methods ////////////////////////////////////////////////////
		private void SetFilledBytes( int newFilledBytes )
		{
			_filledBytes = newFilledBytes;
			if( _filledBytes > bufferSize )
			{
				string msg = "This network buffer has overflown";
				throw new IndexOutOfRangeException( msg );
			}
		}
	}
}
