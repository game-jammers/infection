
//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System;
using System.Collections.Generic;

using blacktriangles;

namespace blacktriangles.Network
{
	[Packet(0)]
	public class UTF8TextPacket
		: Packet
	{
		// members ////////////////////////////////////////////////////////////
		public string text										{ get; protected set; }

		// constructor / initializer //////////////////////////////////////////
		public UTF8TextPacket()
			: base()
		{
			text = System.String.Empty;
		}

		public UTF8TextPacket( string _text )
			: base()
		{
			text = _text;
		}

		// public methods /////////////////////////////////////////////////////
		public override void Decode( RawPacket packet )
		{
			text = System.Text.Encoding.UTF8.GetString( packet.payload.bytes );
		}

		public override RawPacket Encode()
		{
			byte[] bytes = System.Text.Encoding.UTF8.GetBytes( text );
			NetworkBuffer payload = new NetworkBuffer( bytes );

			///###hsmith $TODO Packet IDs!
			RawPacket result = new RawPacket( id, payload );
			return result;
		}
	}
}
