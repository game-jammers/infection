
//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace blacktriangles.Network
{
  public abstract class Packet
  {
    // types //////////////////////////////////////////////////////////////
    public struct Header
    {
      // members ////////////////////////////////////////////////////////
      public int id;
      public int payloadSize;

      // constructor / initializer //////////////////////////////////////
      public Header( int _id, int _payloadSize )
      {
        id = _id;
        payloadSize = _payloadSize;
      }

      // public methods /////////////////////////////////////////////////
      public override string ToString()
      {
        return System.String.Format( "[ id:{0}, payloadSize:{1}]", id, payloadSize );
      }

      // static methods /////////////////////////////////////////////////
      public static Header FromNetworkBuffer( NetworkBuffer buffer )
      {
        int index = 0;

        Header result = new Header();
        result.id = BitConverter.ToInt32( buffer.bytes, index );
        index += sizeof( System.Int32 );
        result.payloadSize = BitConverter.ToInt32( buffer.bytes, index );
        index += sizeof( System.Int32 );

        return result;
      }

      public static NetworkBuffer ToNetworkBuffer( Header header )
      {
        NetworkBuffer result = new NetworkBuffer( Packet.kHeaderSize );
        result.PutBytes( BitConverter.GetBytes( header.id ) );
        result.PutBytes( BitConverter.GetBytes( header.payloadSize ) );
        return result;
      }
    }

    // constants //////////////////////////////////////////////////////////
    public static readonly int kHeaderSize                      = System.Runtime.InteropServices.Marshal.SizeOf( typeof( Header ) );

    // members ////////////////////////////////////////////////////////////
    public int id                                               { get { return packetAttrib.id; } }
    public PacketAttribute packetAttrib                         { get { return GetPacketAttribute(); } }
    private PacketAttribute _packetAttrib                       = null;

    // public methods /////////////////////////////////////////////////////
    public abstract void Decode( RawPacket data );
    public abstract RawPacket Encode();

    public override string ToString()
    {
      return GetType().Name;
    }

    // private methods ////////////////////////////////////////////////////
    private PacketAttribute GetPacketAttribute()
    {
      if( _packetAttrib == null )
      {
        _packetAttrib = AssemblyUtility.GetAttribute<PacketAttribute>( GetType() );
      }

      return _packetAttrib;
    }
  }
}
