//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System.Collections.Generic;
using blacktriangles;

namespace blacktriangles.Network
{
	public class PacketRouter
	{
		// members ////////////////////////////////////////////////////////////
		private Dictionary<int,System.Type> packetTypes			= null;
		private Dictionary<int,System.Delegate> packetHandlers 	= null;
		public bool isInitialized								{ get; private set; }

		// constructor / initializer //////////////////////////////////////////
		public PacketRouter()
		{
			isInitialized = false;
			Initialize();
		}

		public void Initialize()
		{
			if( !isInitialized )
			{
				packetTypes = new Dictionary<int,System.Type>();
				packetHandlers = new Dictionary<int,System.Delegate>();

				System.Type[] packetTypeList = AssemblyUtility.CollectTypesWithAttribute<PacketAttribute>();
				foreach( System.Type type in packetTypeList )
				{
					PacketAttribute packetAttribute = AssemblyUtility.GetAttribute<PacketAttribute>( type );
					packetTypes[ packetAttribute.id ] = type;
				}

				isInitialized = true;
			}
		}

		// public methods /////////////////////////////////////////////////////
		public bool Route( RawPacket rawPacket )
		{
			bool result = false;

			System.Type type = default(System.Type);
			if( packetTypes.TryGetValue( rawPacket.header.id, out type ) )
			{
				Packet packet = System.Activator.CreateInstance( type ) as Packet;
				if( packet != null )
				{
					packet.Decode( rawPacket );
					result = Route( rawPacket.source, packet );
				}
			}

			return result;
		}

		public void AddRoute<PacketType>( System.Func<Connection,PacketType,bool> handler )
			where PacketType: Packet
		{
			PacketAttribute packetAttribute = AssemblyUtility.GetAttribute<PacketAttribute>( typeof(PacketType) );
			if( packetAttribute != null )
			{
				int id = packetAttribute.id;
				System.Delegate result = null;
				if( packetHandlers.TryGetValue( id, out result ) )
				{
					System.Func<Connection,PacketType,bool> castHandlers = (System.Func<Connection,PacketType,bool>)result;
					castHandlers -= handler;
					castHandlers += handler;
					result = (System.Delegate)castHandlers;
				}
				else
				{
					result = (System.Delegate)handler;
				}

				packetHandlers[id] = result;
			}
		}

		// private methods ////////////////////////////////////////////////////
		private bool Route( Connection connection, Packet packet )
		{
			bool result = false;

			System.Delegate handler = null;
			if( packetHandlers.TryGetValue( packet.id, out handler ) )
			{
				result = (bool)handler.DynamicInvoke( connection, packet );
			}

			return result;
		}
    }
}
