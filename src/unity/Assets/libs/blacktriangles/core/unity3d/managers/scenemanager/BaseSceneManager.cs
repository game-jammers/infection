//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;

namespace blacktriangles
{
	public class BaseSceneManager
		: MonoBehaviour
	{
		// members ////////////////////////////////////////////////////////////
		public static BaseSceneManager instance                     { get; protected set; }

		public BaseSceneController sceneController					        { get; private set; }

		// callbacks //////////////////////////////////////////////////////////
		public void OnSceneControllerLoaded( BaseSceneController controller )
		{
			sceneController = controller;
		}

		// unity callbacks ////////////////////////////////////////////////////
		protected virtual void Awake()
		{
			instance = this;
		}

    // utility methods ////////////////////////////////////////////////////////
    protected static SceneManagerType EnsureExists<SceneManagerType>( string prefabPath )
      where SceneManagerType: BaseSceneManager
    {
    	if( instance == null )
			{
				GameObject prefab = Resources.Load( prefabPath, typeof( GameObject ) ) as GameObject;
				GameObject go = Instantiate( prefab );
				instance = go.GetComponent<BaseSceneManager>();
			}

			return instance as SceneManagerType;
    }
	}
}
