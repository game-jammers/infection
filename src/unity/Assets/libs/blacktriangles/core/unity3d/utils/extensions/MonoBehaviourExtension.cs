//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using System.Collections;

namespace blacktriangles
{
  public static class MonoBehaviourExtensions
  {
    public static IEnumerator DelayedCall( System.Action action, float time )
    {
      yield return new WaitForSeconds( time );
      action();
    }

    public static IEnumerator CallAtEndOfFrame( System.Action action )
    {
      yield return new WaitForEndOfFrame();
      action();
    }

  	public static void DelayedCall( this MonoBehaviour self, System.Action action, float time )
  	{
      self.StartCoroutine( DelayedCall( action, time ) );
  	}

    public static void CallAtEndOfFrame( this MonoBehaviour self, System.Action action )
    {
      self.StartCoroutine( CallAtEndOfFrame( action ) );
    }
  }
}
