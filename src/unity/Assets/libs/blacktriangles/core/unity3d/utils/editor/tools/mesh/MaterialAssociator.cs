//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

﻿using UnityEngine;
using UnityEditor;

using System.Linq;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace blacktriangles
{
	public class MaterialAssociator
		: EditorWindow
	{
    // types //////////////////////////////////////////////////////////////////
    [System.Serializable]
    public class MaterialJoin
    {
      public Material material;
      public Texture albedo;
      public Texture normal;
    }

		// constants //////////////////////////////////////////////////////////////
		private const string kMenuPath 						                  = "Tools/blacktriangles/Mesh/Material Associator";
		private const string kTitle								                  = "Material Associator";

    // members ////////////////////////////////////////////////////////////////
		private List<Material> materials							              = new List<Material>();
    private bool foldoutMats                                    = false;
    private Vector2 materialScrollPos                           = Vector2.zero;

    private List<Texture> textures                              = new List<Texture>();
    private bool foldoutTexs                                    = false;
    private Vector2 textureScrollPos                            = Vector2.zero;
    private int totalFilesToProcess                             = 0;
    private int filesProcessed                                  = 0;

    private List<MaterialJoin> joins                            = new List<MaterialJoin>();
    private bool foldoutJoins                                   = false;
    private Vector2 joinScrollPos                               = Vector2.zero;
    private bool showMissingAlbedo                              = false;
    private bool showMissingNormal                              = false;

		// constructor / destructor ///////////////////////////////////////////
		[MenuItem( kMenuPath )]
		private static void OpenWindow()
		{
			MaterialAssociator window = GetWindow<MaterialAssociator>();
			window.titleContent = new GUIContent( kTitle );
			window.Initialize();
		}

		private void Initialize()
		{
			//materials = new List<Material>();
      //textures = new List<Texture>();
		}

		// unity callbacks ////////////////////////////////////////////////////
    protected virtual void Clear()
    {
      materials = new List<Material>();
      textures = new List<Texture>();
      joins = new List<MaterialJoin>();
    }

		protected virtual void OnEnable()
		{
			Initialize();
		}

		protected virtual void OnGUI()
		{
			EditorGUIUtility.OnGUI();

      GUILayout.BeginVertical( GUI.skin.box );
        GUILayout.Label( "Commands" );
				if( GUILayout.Button( "Get Materials" ) )
        {
          LoadMaterials();
        }
        else if( GUILayout.Button( "Find Textures" ) )
        {
          LoadTextures();
        }
        else if( materials.Count > 0 && textures.Count > 0 && GUILayout.Button( "Associate" ) )
        {
          Associate();
        }
        else if( joins.Count > 0 && GUILayout.Button( "Update Materials" ) )
        {
          UpdateMaterials();
        }

        EditorGUILayout.Space();
        GUILayout.Label( "Utilities" );

        if( GUILayout.Button( "Clear" ) )
        {
          Clear();
        }
        else if( GUILayout.Button( "Generate Report" ) )
        {
          GenerateReport();
        }
			GUILayout.EndVertical();

      foldoutMats = EditorGUIUtility.Foldout( foldoutMats, "Materials" );
      if( foldoutMats )
      {
        GUILayout.BeginVertical( GUI.skin.box );
          materialScrollPos = GUILayout.BeginScrollView( materialScrollPos );
            foreach( Material mat in materials )
            {
              EditorGUIUtility.ThinObjectField( System.String.Empty, mat, false );
            }
          GUILayout.EndScrollView();
        GUILayout.EndVertical();
      }

      foldoutTexs = EditorGUIUtility.Foldout( foldoutTexs, "Textures" );
      if( foldoutTexs )
      {
        GUILayout.BeginVertical( GUI.skin.box );
          textureScrollPos = GUILayout.BeginScrollView( textureScrollPos );
            foreach( Texture tex in textures )
            {
              EditorGUIUtility.ThinObjectField( System.String.Empty, tex, false );
            }
          GUILayout.EndScrollView();
        GUILayout.EndVertical();
      }

      foldoutJoins = EditorGUIUtility.Foldout( foldoutJoins, "Associations" );
      if( foldoutJoins )
      {
        showMissingAlbedo = EditorGUILayout.Toggle( "Show Missing Albedo", showMissingAlbedo );
        showMissingNormal = EditorGUILayout.Toggle( "Show Missing Normal", showMissingNormal );

        GUILayout.BeginVertical( GUI.skin.box );
          joinScrollPos = GUILayout.BeginScrollView( joinScrollPos );
            for( int i = 0; i < joins.Count; ++i )
            {
              MaterialJoin matJoin = joins[i];
              if( (!showMissingAlbedo || matJoin.albedo == null) &&
                  (!showMissingNormal || matJoin.normal == null) )
              {
                joins[i] = (MaterialJoin)EditorGUIUtility.ObjectReflectionGUI( matJoin );
              }
            }
          GUILayout.EndScrollView();
        GUILayout.EndVertical();
      }
		}

		// private methods ////////////////////////////////////////////////////
		private void LoadMaterials()
    {
      materials = new List<Material>();
      string matFolder = EditorUtility.OpenFolderPanel( "Select Materials Folder", System.String.Empty, System.String.Empty );
      if( matFolder != System.String.Empty )
      {
        string[] files = System.IO.Directory.GetFiles( matFolder );
        foreach( string file in files )
        {
          if( file.EndsWith( ".mat" ) )
          {
            string relativePath = FileUtility.MakePathRelativeToAssetDir( file );
            Material newMat = AssetDatabase.LoadAssetAtPath<Material>( relativePath );
            materials.Add( newMat );
          }
        }
      }
    }

    private void LoadTextures()
    {
      textures = new List<Texture>();
      string rootFolder = EditorUtility.OpenFolderPanel( "Select Root Textures Folder", System.String.Empty, System.String.Empty );
      if( rootFolder != System.String.Empty )
      {
        totalFilesToProcess = 0;
        filesProcessed = 0;
        EditorUtility.DisplayProgressBar( "Loading Textures...", "Loading Textures recursively from " + rootFolder, 0.0f );
        ProcessTextureFolder( rootFolder );
        EditorUtility.ClearProgressBar();
      }
    }

    private void ProcessTextureFolder( string path )
    {
      if( filesProcessed > 0 )
      {
        EditorUtility.DisplayProgressBar( "Loading Textures...", "Processing Directory " + path, (float)totalFilesToProcess / (float)filesProcessed );
      }

      string[] dirs = System.IO.Directory.GetDirectories( path );
      foreach( string dir in dirs )
      {
        string[] countFiles = System.IO.Directory.GetFiles( dir );
        totalFilesToProcess += countFiles.Length;
        ProcessTextureFolder( dir );
      }

      string[] files = System.IO.Directory.GetFiles( path );
      foreach( string file in files )
      {
        string texPath = FileUtility.MakePathRelativeToAssetDir( System.IO.Path.Combine( path, file ) );
        Texture texture = AssetDatabase.LoadAssetAtPath<Texture>( texPath );
        if( texture != null )
        {
          textures.Add( texture );
        }
        ++filesProcessed;
      }
    }

    private string[] BuildMatches( string baseName )
    {
      return new string[] {
        baseName,
        Regex.Replace( baseName, @"Costume", "Cos", RegexOptions.None ),
        Regex.Replace( baseName, @"\d*", "", RegexOptions.None ),
        Regex.Replace( Regex.Replace( baseName, @"Costume", "Cos", RegexOptions.None ), @"\d*", "", RegexOptions.None ),
      };
    }

    private Texture FindTexture( string[] matches )
    {
      System.Text.StringBuilder ss = new System.Text.StringBuilder();
      foreach( string match in matches )
      {
        ss.Append( "Failed to match: " );
        ss.Append( match );
        ss.Append( "\n" );
        Texture res = textures.Find( (x)=>{
            ss.Append( "\t" );
            ss.Append( match );
            ss.Append( " == " );
            ss.Append( x.name );
            ss.Append( "\n" );
            return x.name == match;
          });
        if( res != null ) return res;
      }

      Debug.Log( ss.ToString() );
      return null;
    }

    private void Associate()
    {
      int processed = 0;
      foreach( Material mat in materials )
      {
        float perc = ( processed > 0 ) ? ((float)materials.Count/(float)processed) : 0.0f;
        EditorUtility.DisplayProgressBar( "Finding Associated Textures...", "Finding Textures for " + mat.name, perc );

        string[] albedoMatches = BuildMatches( mat.name );
        string[] normalMatches = BuildMatches( mat.name + "(NRM)" );

        MaterialJoin matJoin = new MaterialJoin();
        matJoin.material = mat;
        matJoin.albedo = FindTexture( albedoMatches );
        matJoin.normal = FindTexture( normalMatches );

        joins.Add( matJoin );
        ++processed;
      }
      EditorUtility.ClearProgressBar();
    }

    private void UpdateMaterials()
    {
      foreach( MaterialJoin matJoin in joins )
      {
        matJoin.material.EnableKeyword( "_NORMALMAP" );
        matJoin.material.SetTexture( "_MainTex", matJoin.albedo );
        matJoin.material.SetTexture( "_BumpMap", matJoin.normal );
        matJoin.material.SetFloat( "_Glossiness", 0.0f );

      }
    }

    private void GenerateReport()
    {
      string filepath = EditorUtility.SaveFilePanel( "Save Report", System.String.Empty, "MissingTextures", "txt" );
      if( filepath != System.String.Empty )
      {
        List<MaterialJoin> missingBoth = new List<MaterialJoin>();
        List<MaterialJoin> missingAlbedo = new List<MaterialJoin>();
        List<MaterialJoin> missingNormal = new List<MaterialJoin>();

        foreach( MaterialJoin mjoin in joins )
        {
          if( mjoin.albedo == null && mjoin.normal == null )
          {
            missingBoth.Add( mjoin );
          }
          else if( mjoin.albedo == null )
          {
            missingAlbedo.Add( mjoin );
          }
          else if( mjoin.normal == null )
          {
            missingNormal.Add( mjoin );
          }
        }

        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.Append( "MISSING BOTH\n=========================\n" );
        foreach( MaterialJoin mjoin in missingBoth )
        {
          sb.Append( mjoin.material.name );
          sb.Append( "\n" );
        }

        sb.Append( "\n" );
        sb.Append( "MISSING ALBEDO\n=========================\n" );
        foreach( MaterialJoin mjoin in missingAlbedo )
        {
          sb.Append( mjoin.material.name );
          sb.Append( "\n" );
        }

        sb.Append( "\n" );
        sb.Append( "MISSING NORMAL\n=========================\n" );
        foreach( MaterialJoin mjoin in missingNormal )
        {
          sb.Append( mjoin.material.name );
          sb.Append( "\n" );
        }

        System.IO.File.WriteAllText( filepath, sb.ToString() );
      }
    }

	}
}
