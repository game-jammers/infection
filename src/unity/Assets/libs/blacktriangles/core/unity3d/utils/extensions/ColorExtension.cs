//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;

namespace blacktriangles
{
  public static class ColorExtension
  {
  	public static string ToHexString( this Color self )
  	{
      byte[] bytes = { self.r.ToByte(), self.g.ToByte(), self.b.ToByte() };
  		return "#" + System.BitConverter.ToString(bytes).Replace("-","");
  	}
  }
}
