//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEditor;

namespace blacktriangles
{
	public static class EditorIcons
	{
		public static Texture2D Brush 							{ get; private set; }
		public static Texture2D Paintbucket						{ get; private set; }
		public static Texture2D Eyedropper						{ get; private set; }
		public static Texture2D New								{ get; private set; }
		public static Texture2D Save							{ get; private set; }
		public static Texture2D Load							{ get; private set; }
		public static Texture2D Edit							{ get; private set; }
		public static Texture2D Settings						{ get; private set; }
		public static Texture2D Settings2						{ get; private set; }
		public static Texture2D Navigation						{ get; private set; }
		public static Texture2D NewWindow						{ get; private set; }
		public static Texture2D Earth							{ get; private set; }
		public static Texture2D Globe							{ get; private set; }
		public static Texture2D Tiles							{ get; private set; }
		public static Texture2D Pattern							{ get; private set; }
		public static Texture2D ExpandVertical					{ get; private set; }
		public static Texture2D ExpandHorizontal				{ get; private set; }
		public static Texture2D CollapseVertical				{ get; private set; }
		public static Texture2D CollapseHorizontal				{ get; private set; }
		public static Texture2D Plus							{ get; private set; }
		public static Texture2D Minus							{ get; private set; }
		public static Texture2D Object							{ get; private set; }
		public static Texture2D Event							{ get; private set; }
		public static Texture2D ArrowUp							{ get; private set; }
		public static Texture2D ArrowDown						{ get; private set; }
		public static Texture2D ArrowLeft						{ get; private set; }
		public static Texture2D ArrowRight						{ get; private set; }
		public static Texture2D Plug							{ get; private set; }
		public static Texture2D Socket							{ get; private set; }
		public static Texture2D Connection						{ get; private set; }
		public static Texture2D Info							{ get; private set; }
		public static Texture2D Check							{ get; private set; }
		public static Texture2D X								{ get; private set; }

		public static void LoadIcons()
		{
			Brush = EditorConfig.GetIcon( "brush" );
			Paintbucket = EditorConfig.GetIcon( "paintbucket" );
			Eyedropper = EditorConfig.GetIcon( "eyedropper" );
			New = EditorConfig.GetIcon( "file_new" );
			Save = EditorConfig.GetIcon( "file_save" );
			Load = EditorConfig.GetIcon( "file_load" );
			Edit = EditorConfig.GetIcon( "edit" );
			Settings = EditorConfig.GetIcon( "settings" );
			Settings2 = EditorConfig.GetIcon( "settings2" );
			Navigation = EditorConfig.GetIcon( "navigation" );
			NewWindow = EditorConfig.GetIcon( "new_window" );
			Earth = EditorConfig.GetIcon( "earth" );
			Globe = EditorConfig.GetIcon( "globe" );
			Tiles = EditorConfig.GetIcon( "tiles" );
			Pattern = EditorConfig.GetIcon( "pattern" );
			ExpandVertical = EditorConfig.GetIcon( "expandv" );
			CollapseVertical = EditorConfig.GetIcon( "collapsev" );
			ExpandHorizontal = EditorConfig.GetIcon( "expandh" );
			CollapseHorizontal = EditorConfig.GetIcon( "collapseh" );
			Plus = EditorConfig.GetIcon( "plus" );
			Minus = EditorConfig.GetIcon( "remove" );
			Object = EditorConfig.GetIcon( "object" );
			Event = EditorConfig.GetIcon( "event" );
			ArrowUp = EditorConfig.GetIcon( "arrow_up" );
			ArrowDown = EditorConfig.GetIcon( "arrow_down" );
			ArrowLeft = EditorConfig.GetIcon( "arrow_left" );
			ArrowRight = EditorConfig.GetIcon( "arrow_right" );
			Plug = EditorConfig.GetIcon( "plug" );
			Socket = EditorConfig.GetIcon( "socket" );
			Connection = EditorConfig.GetIcon( "connection" );
			Info = EditorConfig.GetIcon( "info" );
			Check = EditorConfig.GetIcon( "check" );
			X = EditorConfig.GetIcon( "x" );
		}
	}
}
