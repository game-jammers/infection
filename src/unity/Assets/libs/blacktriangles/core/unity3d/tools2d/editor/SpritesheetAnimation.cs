//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEditor;
using blacktriangles;

using System.Collections.Generic;

namespace blacktriangles.Tools2d
{
	public class SpritesheetAnimation
		: IJsonSerializable
	{
		// members ////////////////////////////////////////////////////////////
		public string name										= "New Animation";
		public List<SpritesheetAnimationFrame> frames			= new List<SpritesheetAnimationFrame>();
		public float duration									{ get { return GetDuration(); } }

		// public methods /////////////////////////////////////////////////////
		public SpritesheetAnimationFrame GetFrameAtTime( float time, bool looping )
		{
			if( frames == null || frames.Count <= 0 ) return null;

			SpritesheetAnimationFrame result = null;

			float totalDuration = duration;
			if( looping )
			{
				time = time % totalDuration;
			}

			if( time >= totalDuration )
			{
				result = frames[ frames.Count-1 ];
			}
			else
			{
				float frameEndTime = 0f;
				foreach( SpritesheetAnimationFrame frame in frames )
				{
					frameEndTime += frame.duration;
					if( frameEndTime > time )
					{
						result = frame;
						break;
					}
				}
			}

			return result;
		}

		public JsonObject ToJson()
		{
			JsonObject result = new JsonObject();
			result["name"] = name;
			result["frames"] = frames;
			return result;
		}

		public void FromJson( JsonObject json )
		{
			SpritesheetAnimationFrame[] defs = json.GetField<SpritesheetAnimationFrame[]>( "frames" );
			name = json.GetField<string>( "name" );
			frames = new List<SpritesheetAnimationFrame>( defs );
		}

		// private methods ////////////////////////////////////////////////////
		private float GetDuration()
		{
			float result = 0f;
			foreach( SpritesheetAnimationFrame frame in frames )
			{
				result += frame.duration;
			}
			return result;
		}
	}
}
