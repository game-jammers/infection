//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEditor;
using blacktriangles;

using System.Collections.Generic;

namespace blacktriangles
{
	public class JsonDbEditor
		: EditorWindow
	{
		// constants //////////////////////////////////////////////////////////
		private const string kMenuPath 							= "Tools/blacktriangles/Database/JsonDbEditor";
		private const string kTitle								= "JsonDb Editor";

		// members ////////////////////////////////////////////////////////////
		System.Type[] jsonObjectTypes							= null;
		string[] jsonObjectNames								= null;
		int selectedType										= -1;

		List<IJsonSerializable> objects							= null;

		// constructor / initializer //////////////////////////////////////////
		[MenuItem( kMenuPath )]
		public static void OpenWindow()
		{
			JsonDbEditor window = EditorWindow.GetWindow<JsonDbEditor>();
			window.titleContent = new GUIContent( kTitle );
			window.Initialize();
		}

		private void Initialize()
		{
			System.Type interfaceType = typeof( IJsonSerializable );
			jsonObjectTypes = AssemblyUtility.CollectTypes( AssemblyUtility.GetGameAssembly(), (type)=>{
				return interfaceType.IsAssignableFrom( type )
					&& type.IsAbstract == false
					&& type.IsInterface == false
					&& type.GetConstructor( System.Type.EmptyTypes ) != null
					&& type.IsSubclassOf( typeof( UnityEngine.MonoBehaviour ) ) == false
					&& type.IsGenericType == false;
			});

			jsonObjectNames = System.Array.ConvertAll<System.Type,string>( jsonObjectTypes, (type)=>{ return type.Name; } );
			objects = new List<IJsonSerializable>();
		}

		// unity callbacks ////////////////////////////////////////////////////
		protected virtual void OnEnable()
		{
			Initialize();
		}

		protected virtual void OnGUI()
		{
			if( objects.Count <= 0 )
			{
				selectedType = EditorGUILayout.Popup( "Schema", selectedType, jsonObjectNames );
			}
			else if( jsonObjectNames.IsValidIndex( selectedType ) )
			{
				GUILayout.Label( jsonObjectNames[ selectedType ] );
			}

			if( jsonObjectTypes.IsValidIndex( selectedType ) && GUILayout.Button( "Add" ) )
			{
				IJsonSerializable newItem = System.Activator.CreateInstance( jsonObjectTypes[ selectedType ] ) as IJsonSerializable;
				objects.Add( newItem );
			}
			foreach( IJsonSerializable obj in objects )
			{
				GUILayout.BeginVertical( GUI.skin.box );
					EditorGUIUtility.ObjectReflectionGUI( obj.GetType().Name, obj );
				GUILayout.EndVertical();
			}

			GUILayout.BeginVertical( GUI.skin.box );
				if( objects.Count > 0 && GUILayout.Button( "Save" ) )
				{
					string path = EditorUtility.SaveFilePanelInProject( "Save JsonDb", jsonObjectNames[selectedType], "json", "Save JsonDb" );
					if( System.String.IsNullOrEmpty( path ) == false )
					{
						JsonObject json = new JsonObject();
						json["type"] = jsonObjectNames[ selectedType ];
						json["entries"] = objects;
						System.IO.File.WriteAllText( path, json.ToString() );
					}
				}

				if( GUILayout.Button( "Load" ) )
				{
					string path = EditorUtility.OpenFilePanel( "Load JsonDb", "Assets/", "json" );
					if( System.String.IsNullOrEmpty( path ) == false )
					{
						string jsonStr = System.IO.File.ReadAllText( path );
						JsonObject json = JsonObject.FromString( jsonStr );
						string type = json.GetField<string>("type");
						selectedType = jsonObjectNames.IndexOf( type );
						if( jsonObjectTypes.IsValidIndex( selectedType ) )
						{
							System.Type innerType = jsonObjectTypes[selectedType];
							JsonObject[] jsonArr = json.GetField<JsonObject[]>( "entries" );
							foreach( JsonObject itemJson in jsonArr )
							{
								IJsonSerializable item = System.Activator.CreateInstance( innerType ) as IJsonSerializable;
								if( item != null )
								{
									item.FromJson( itemJson );
									objects.Add( item );
								}
							}
						}
					}
				}
			GUILayout.EndVertical();
		}
	}
}
