//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using System.Collections.Generic;

namespace blacktriangles
{
	public class JsonDb<EntryType>
		where EntryType: IJsonSerializable
	{
		// members ////////////////////////////////////////////////////////////
		protected EntryType[] entries							= null;

		// constructor / initializer //////////////////////////////////////////
		public virtual void Initialize( JsonObject json )
		{
			entries = json.GetField<EntryType[]>( "entries" );
		}

		// public methods /////////////////////////////////////////////////////
		public EntryType GetEntry( System.Predicate<EntryType> predicate )
		{
			foreach( EntryType entry in entries )
			{
				if( predicate( entry ) )
				{
					return entry;
				}
			}

			return default(EntryType);
		}

		public EntryType[] GetEntries( System.Predicate<EntryType> predicate )
		{
			List<EntryType> result = new List<EntryType>();
			foreach( EntryType entry in entries )
			{
				if( predicate( entry ) )
				{
					result.Add(entry);
				}
			}

			return result.ToArray();
		}
	}
}
