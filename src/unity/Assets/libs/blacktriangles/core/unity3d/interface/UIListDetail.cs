//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEngine.UI;
using blacktriangles;

using System.Collections;
using System.Collections.Generic;

namespace blacktriangles
{
	public class UIListDetail
		: UIElement
	{
	    // members ////////////////////////////////////////////////////////////
	    public UIListElement item                               { get; private set; }

	    // public methods /////////////////////////////////////////////////////
	    public void Refresh( UIListElement _item )
	    {
	    	if( item != _item )
	    	{
	        	item = _item;
	        	Refresh();
			}
	    }
	}
}
