//=============================================================================
//
// (C) BLACKTRIANGLES 2016
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using blacktriangles;

using System.Collections;

namespace blacktriangles
{
	[RequireComponent( typeof( RectTransform ) )]
	public class UIElement
		: MonoBehaviour
	{
		// members ////////////////////////////////////////////////////////////////
		public RectTransform rectTransform                      { get; private set; }
		public CanvasGroup canvasGroup                          { get; private set; }
		public UIElementStateSet state							{ get { return _state; } }
		public bool isInitialized                               { get; private set; }

		[SerializeField] private UIElementStateSet _state;

		// constructor / initializer //////////////////////////////////////////////
		public virtual void Initialize()
		{
			isInitialized = true;
		}

		// public methods /////////////////////////////////////////////////////////
		public virtual void Show( float time = 1.0f, System.Action callback = null )
		{
			Fade( true, time, callback );
		}

		public virtual void Hide( float time = 1.0f, System.Action callback = null )
		{
			Fade( false, time, callback );
		}

		public virtual void Fade( bool fadeIn, float time, System.Action callback )
		{
			System.Action onFinished = ()=>{
					_state.visible.active = fadeIn;
					if( callback != null ) callback();
				};

			if( canvasGroup == null )
			{
				gameObject.SetActive( fadeIn );
				onFinished();
			}
			else
			{
				if( time.IsApproximately( 0.0f ) )
				{
					canvasGroup.alpha = fadeIn ? 1.0f : 0.0f;
					gameObject.SetActive( fadeIn );
					onFinished();
				}
				else
				{
					gameObject.SetActive( true );
					StartCoroutine( FadeCoroutine( fadeIn, time, onFinished ) );
				}
			}
		}

		public virtual void Refresh()
		{
			state.RefreshColors();
		}

		public virtual void Select()
		{
			SelectGameObject( gameObject );
		}

		public virtual void SelectGameObject( GameObject go )
		{
			StartCoroutine( SelectGameObjectAtEndOfFrame( go ) );
		}

		// protected methods //////////////////////////////////////////////////////
		protected virtual void OnStartAnimation()
		{
		}

		protected virtual void OnEndAnimation()
		{
		}

		// unity callbacks ////////////////////////////////////////////////////////
		protected virtual void Awake()
		{
			rectTransform = GetComponent<RectTransform>();
			canvasGroup = GetComponent<CanvasGroup>();
		}

		// private methods ////////////////////////////////////////////////////////
		private IEnumerator FadeCoroutine( bool fadeIn, float time, System.Action callback )
		{
			DebugUtility.Assert( callback != null, "Cannot call FadeCoroutine without a callback" );
			OnStartAnimation();

			float start = fadeIn ? 0f : 1f;
			float end = fadeIn ? 1f : 0f;

			float elapsed = 0f;
			while( elapsed < time )
			{
				elapsed += Time.deltaTime;
				float perc = elapsed / time;
				canvasGroup.alpha = Mathf.Lerp( start, end, perc );
				yield return new WaitForSeconds(0f);
			}

			canvasGroup.alpha = end;
			gameObject.SetActive( fadeIn );
			OnEndAnimation();
			callback();
		}

		private IEnumerator SelectGameObjectAtEndOfFrame( GameObject go )
		{
			yield return new WaitForEndOfFrame();
			EventSystem.current.SetSelectedGameObject( go );
		}
	}
}
