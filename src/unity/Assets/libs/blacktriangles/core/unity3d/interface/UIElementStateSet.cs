//=============================================================================
//
// (C) BLACKTRIANGLES 2016
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEngine.UI;

namespace blacktriangles
{
	[System.Serializable]
	public class UIElementStateSet
	{
		// types //////////////////////////////////////////////////////////////
		[System.Serializable]
		public class State
		{
			public bool active									= false;
			public UnityEngine.Color color 						= Color.white;
		}

		// members ////////////////////////////////////////////////////////////
		public State visible;
		public State disabled;
		public State highlighted;

		public bool useColors									= false;
		public Graphic mainGraphic								= null;
		public Graphic[] secondaryGraphics						= null;

		public State current									{ get { return GetCurrentState(); } }

		// public methods /////////////////////////////////////////////////////
		public void RefreshColors()
		{
			if( useColors == false ) return;

			if( mainGraphic != null )
			{
				SetColor( mainGraphic, current.color );
			}

			foreach( Graphic secondary in secondaryGraphics )
			{
				SetColor( secondary, current.color );
			}
		}

		// private methods ////////////////////////////////////////////////////
		private void SetColor( Graphic graphic, Color color )
		{
			#if USE_TMPRO
				TMPro.TMP_Text text = graphic as TMPro.TMP_Text;
				if( text == null )
				{
					graphic.color = color;
				}
				else
				{
					text.color = color;
				}
			#else
				graphic.color = color;
			#endif
		}

		private State GetCurrentState()
		{
			if( visible.active )
			{
				if( highlighted.active )
				{
					return highlighted;
				}

				return visible;
			}

			return disabled;
		}
	}
}
