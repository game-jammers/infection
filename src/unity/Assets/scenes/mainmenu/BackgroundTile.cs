//=============================================================================
//
// (C) BLACKTRIANGLES 2016
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEngine.UI;
using blacktriangles;
using blacktriangles.Input;

namespace blacktriangles.Mutant
{
    public class BackgroundTile
        : MonoBehaviour
    {
        // members ////////////////////////////////////////////////////////////
        public float size                                       = 0.0f;

        // unity callbacks ////////////////////////////////////////////////////
        protected virtual void Awake()
        {
            float min = Mathf.Infinity;
            float max = 0.0f;

            SpriteRenderer[] sprites = transform.GetComponentsInChildren<SpriteRenderer>();
            foreach( SpriteRenderer rend in sprites )
            {
                min = Mathf.Min( rend.bounds.min.y, min );
                max = Mathf.Max( rend.bounds.max.y, max );
            }

            size = max - min;
        }
    }
}
