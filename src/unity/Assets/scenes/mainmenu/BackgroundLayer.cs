//=============================================================================
//
// (C) BLACKTRIANGLES 2016
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEngine.UI;
using blacktriangles;
using blacktriangles.Input;

namespace blacktriangles.Mutant
{
    public class BackgroundLayer
        : MonoBehaviour
    {
        // members ////////////////////////////////////////////////////////////
        public BackgroundTile[] tileset                         = null;
        public Deque<BackgroundTile> tiles                      = new Deque<BackgroundTile>();
        public int maxActiveTile                                = 5;
        public float scrollSpeed                                = 10.0f;

        public float size                                       = 0.0f;

        // unity callbacks ////////////////////////////////////////////////////
        protected virtual void Awake()
        {
            for( int i = 0; i < maxActiveTile; ++i )
            {
                AddRandomTile();
            }
        }

        protected virtual void Update()
        {
            transform.position -= new Vector3( 0.0f, scrollSpeed, 0.0f ) * Time.deltaTime;
        }

        protected virtual void LateUpdate()
        {
            BackgroundTile tile = tiles.GetBack();
            if( transform.position.y < (-tile.size) )
            {
                RemoveBackTile();
                AddRandomTile();
            }
        }

        // private methods ////////////////////////////////////////////////////
        private int val = 0;
        BackgroundTile AddRandomTile()
        {
            BackgroundTile result = Instantiate( tileset[(val++)%tileset.Length], Vector3.zero, Quaternion.identity ) as BackgroundTile;
            result.name = val.ToString();
            AddTile( result );
            return result;
        }

        void AddTile( BackgroundTile tile )
        {
            tile.transform.SetParent( transform, false );
            tile.transform.position += new Vector3( 0f, size, 0f );
            size += tile.size;
            tiles.AddToFront( tile );
        }

        void RemoveBackTile()
        {
            BackgroundTile tile = tiles.RemoveFromBack();
            size -= tile.size;
            Vector3 offset = new Vector3( 0.0f, tile.size, 0.0f );
            foreach( BackgroundTile t in tiles )
            {
                t.transform.position -= offset;
            }

            transform.position += offset;
            Destroy( tile.gameObject );
        }
    }
}
