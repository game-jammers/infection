//=============================================================================
//
// (C) BLACKTRIANGLES 2016
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEngine.UI;
using blacktriangles;
using blacktriangles.Input;

namespace blacktriangles.Mutant
{
    public class Background
        : MonoBehaviour
    {
        // members ////////////////////////////////////////////////////////////
        public BackgroundLayer[] layers                         { get; private set; }

        // unity callbacks ////////////////////////////////////////////////////
        protected virtual void Awake()
        {
            layers = transform.GetComponentsInChildren<BackgroundLayer>();
        }
    }
}
