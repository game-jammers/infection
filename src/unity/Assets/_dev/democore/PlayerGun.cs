﻿using UnityEngine;
using System.Collections;

public class PlayerGun
    : MonoBehaviour
{

    public GameObject Rotator;
    public float UpDown = 1f;
    public float UpDownSpeed = 0.3f;
    public float Radius = 0.5f;
    public float RadiusSpeed = 1f;

	// Update is called once per frame
	void FixedUpdate()
    {
        // orbit //
        float x = Radius * Mathf.Sin( Time.time * RadiusSpeed) + transform.parent.position.x;
        float y = transform.transform.position.y;
        float z = Radius * Mathf.Cos( Time.time * RadiusSpeed) + transform.parent.position.z;
        Rotator.transform.position = new Vector3( x, y, z );

        // rotate to face mouse //
        FixedRotation();
    }

    public static Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Quaternion angle)
    {
        return angle * (point - pivot) + pivot;
    }

    private void FixedRotation()
    {
        //http://wiki.unity3d.com/index.php?title=LookAtMouse
        //Rotation
        // Generate a plane that intersects the transform's position with an upwards normal.
        Plane playerPlane = new Plane(Vector3.up, Rotator.transform.position);

        // Generate a ray from the cursor position
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        float hitdist = 0.0f;
        // If the ray is parallel to the plane, Raycast will return false.
        if (playerPlane.Raycast(ray, out hitdist))
        {
            // Get the point along the ray that hits the calculated distance.
            Vector3 targetPoint = ray.GetPoint(hitdist);
            // Determine the target rotation.  This is the rotation if the transform looks at the target point.
            Quaternion targetRotation = Quaternion.LookRotation(targetPoint - Rotator.transform.position);
            Rotator.transform.localRotation = Quaternion.Slerp(Rotator.transform.rotation, targetRotation, 10.0f * Time.deltaTime);
        }
    }
}
