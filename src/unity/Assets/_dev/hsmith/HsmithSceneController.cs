//
// Game Jammers 2016
//

using blacktriangles;
using blacktriangles.Input;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace GameJammers.Infection
{
	public class HsmithSceneController
		: DungeonSceneController
	{
		// unity callbacks ////////////////////////////////////////////////////
		protected virtual void Start()
		{
			BuildDungeon( ()=>{
				Debug.Log( "Complete" );
			});
		}
	};
}
