# Game Jammers #1 - Infection
November 4, 2016

We have rolled the virtual dice and are doing **INFECTION** for this game jam.


We're discussing a top down twin stick shooter similar to games like Nuclear Throne and Geometry Wars, where the player is a virus attempting to murder a host.  Fight through a randomly generated level to get to an organ protected by various bosses (inspired by medicines and vaccines), mutate and evolve new weapons and abilities!


## Tools
- Unity v5.4.2f2


## Git Tutorial
- Howard Was Here!
